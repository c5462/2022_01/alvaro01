report 50103 "ALVEtiquetaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './layout/Etiqueta.rdlc';
    //UseRequestPage = false;

    dataset
    {
        dataitem("Sales Shipment Line"; "Sales Shipment Line")
        {
            column(No_SalesShipmentLine; "No.")
            {
            }
            column(DocumentNo_SalesShipmentLine; "Document No.")
            {
            }
            column(Description_SalesShipmentLine; Description)
            {
            }
            dataitem(Integer; Integer)
            {
                column(Number_Integer; Number)
                {

                }

                trigger OnPreDataItem()
                begin
                    Integer.SetRange(Number, 0, "Sales Shipment Line".Quantity);
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                }
            }
        }

        actions
        {
            area(processing)
            {
            }
        }
    }
}