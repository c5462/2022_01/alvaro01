page 50103 "ALVListaConfiguracionC01"
{
    ApplicationArea = All;
    Caption = 'Lista Configuración de la App 01 del Curso';
    PageType = List;
    SourceTable = ALVConfiguracionC01;
    UsageCategory = Lists;
    ShowFilter = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Id; Rec.Id)
                {
                    ToolTip = 'Specifies the value of the Id. Registro field.';
                    ApplicationArea = All;
                }
                field(Cod2; Rec.Cod2)
                {
                    ToolTip = 'Specifies the value of the Código 2 field.';
                    ApplicationArea = All;
                }
                field(CodClienteWeb; Rec.CodClienteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por Web';
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the CodTabla field.';
                    ApplicationArea = All;
                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    ToolTip = 'Specifies the value of the Color de Fondo field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    ToolTip = 'Specifies the value of the Color de Letra field.';
                    ApplicationArea = All;
                }
                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Specifies the value of the NombreCliente field.';
                    ApplicationArea = All;
                }
                field(Stock; Rec.Stock)
                {
                    ToolTip = 'Especifica el stock disponible';
                    ApplicationArea = All;
                }
                field(TextoReg; Rec.TextoReg)
                {
                    ToolTip = 'Specifies the value of the Texto Registro field.';
                    ApplicationArea = All;
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo de Documento field.';
                    ApplicationArea = All;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tabla Condicional field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
