/// <summary>
/// Report ALVLineasVentaCompraC01 (ID 50105).
/// </summary>
report 50105 "ALVLineasVentaCompraC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC; //Indicas de que tipo quieres que sea el informe
    RDLCLayout = './layout/LineasVentaCompra.rdlc';
    AdditionalSearchTerms = 'Listado de Lineas Ventas/Compras';
    Caption = 'Listado de Lineas Ventas/Compras';

    dataset
    {
        dataitem(Cliente; Customer) //Nuestro item productos dentro del área del informe
        {
            column(No_Cliente; "No.")
            {
            }
            column(Name_Cliente; Name)
            {
            }
            column(Name_Empresa; rEmpresa.Name)
            {
            }
            column(Address_Empresa; rEmpresa.Address)
            {
            }

            dataitem(LineasCliente; "Sales Invoice Line") //Ventas de mis productos
            {
                Dataitemlink = "Sell-to Customer No." = field("No.");
                column(No_LineasCliente; "No.")
                {
                }
                column(Cantidad_LineasCliente; Quantity)
                {
                }
                column(Importe_LineasCliente; Amount)
                {
                }
                column(No_ProductoV; rProducto."No.")
                {
                }
                column(Description_ProductoV; rProducto.Description)
                {
                }
                trigger OnAfterGetRecord()
                begin
                    if not rProducto.Get("No.") then begin
                        Clear(rProducto);
                    end;
                end;
            }
        }
        dataitem(Proveedor; Vendor) //Nuestro item productos dentro del área del informe
        {
            column(No_Proveedor; "No.")
            {
            }
            column(Name_Proveedor; Name)
            {
            }
            dataitem(LineasProveedor; "Purch. Inv. Line") //Compras de mis productos
            {
                Dataitemlink = "Buy-from Vendor No." = field("No.");
                column(No_LineasProveedor; "No.")
                {
                }
                column(Cantidad_LineasProveedor; Quantity)
                {
                }
                column(Importe_LineasProveedor; Amount)
                {
                }
                column(No_ProductoC; rProducto."No.")
                {
                }
                column(Description_ProductoC; rProducto.Description)
                {
                }
                trigger OnAfterGetRecord()
                begin
                    if not rProducto.Get("No.") then begin
                        Clear(rProducto);
                    end;
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }
    }

    trigger OnPreReport()
    begin
        if not rEmpresa.Get() then begin
            Clear(rEmpresa);
        end;
    end;

    //Zona de variables globales
    var
        rEmpresa: Record "Company Information";
        rProducto: Record Item;
}