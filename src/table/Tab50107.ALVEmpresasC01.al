table 50107 "ALVEmpresasC01"
{
    Caption = 'Listado de Empresas';
    DataClassification = OrganizationIdentifiableInformation;

    fields
    {
        field(1; Id; Text[30])
        {
            Caption = 'Id';
            DataClassification = OrganizationIdentifiableInformation;
            Editable = false;
        }
        field(2; NombreEmpresa; Text[30])
        {
            Caption = 'Empresa';
            DataClassification = OrganizationIdentifiableInformation;
            Editable = false;
        }
        field(3; "Num_Clientes"; Integer)
        {
            Caption = 'Nº Clientes';
            DataClassification = OrganizationIdentifiableInformation;
            Editable = false;
        }
        field(4; "Num_Proveedores"; Integer)
        {
            Caption = 'Nº Proveedores';
            DataClassification = OrganizationIdentifiableInformation;
            Editable = false;
        }
        field(5; "Num_Cuentas"; Integer)
        {
            Caption = 'Nº Cuentas';
            DataClassification = OrganizationIdentifiableInformation;
            Editable = false;
        }
        field(6; "Num_Productos"; Integer)
        {
            Caption = 'Nº Productos';
            DataClassification = OrganizationIdentifiableInformation;
            Editable = false;
        }
    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
}