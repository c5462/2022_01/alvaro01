codeunit 50101 "ALVVariablesGlobalesAppC01"
{
    SingleInstance = true;
    /// <summary>
    /// SET SWITCHF
    /// </summary>
    /// <param name="pValor">Boolean.</param>
    procedure SwitchF(pValor: Boolean)
    begin
        xSwitch := pValor;
    end;

    /// <summary>
    /// GET SWITCHF
    /// </summary>
    /// <returns>Return value of type Boolen.</returns>
    procedure SwitchF(): Boolean
    begin
        exit(xSwitch);
    end;

    /// <summary>
    /// SET NOMBREF
    /// </summary>
    /// <param name="pNombre">Text[100].</param>
    procedure NombreF(pNombre: Text[100])
    begin
        xName := pNombre;
    end;

    /// <summary>
    /// GET NOMBREF
    /// </summary>
    /// <returns>Return value of type Text[100].</returns>
    procedure NombreF(): Text[100]
    begin
        exit(xName);
    end;

    var
        xSwitch: Boolean;
        xName: Text[100];
}
