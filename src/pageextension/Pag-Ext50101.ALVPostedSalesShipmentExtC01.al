pageextension 50101 "ALVPostedSalesShipmentExtC01" extends "Posted Sales Shipment"
{
    actions
    {
        addlast(Reporting)
        {
            action(ALVEtiquetasC01)
            {
                Caption = 'Etiqueta';
                ApplicationArea = all;
                trigger OnAction()
                var
                    rlSalesShipmentLines: Record "Sales Shipment Line";
                    replEtiquetas: Report ALVEtiquetaC01;
                begin
                    rlSalesShipmentLines.SetRange("Document No.", Rec."No.");
                    replEtiquetas.SetTableView(rlSalesShipmentLines);
                    replEtiquetas.RunModal();
                    Report.Run(50207, true, true, rlSalesShipmentLines);
                end;
            }
        }
    }
}
