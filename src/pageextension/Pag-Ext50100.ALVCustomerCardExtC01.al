/// <summary>
/// PageExtension "ALVCustomerCardExtC01" (ID 50100) extends Record Customer Card.
/// </summary>
pageextension 50100 ALVCustomerCardExtC01 extends "Customer Card"
{
    layout
    {
        addlast(content)
        {
            group(ALVInfoCovidC01)
            {
                Caption = 'Información Covid';

                field(ALVNoVacunasC01; Rec.ALVNoVacunasC01)
                {
                    ToolTip = 'Especifica el número de vacunas que tiene el cliente';
                    ApplicationArea = All;
                }
                field(ALVFechaUltVacunaC01; Rec.ALVFechaUltVacunaC01)
                {
                    ToolTip = 'Especifica la fecha en la que se puso la última vacuna';
                    ApplicationArea = All;
                }
                field(ALVTipoVacunaC01; Rec.ALVTipoVacunaC01)
                {
                    ToolTip = 'Especifica el tipo de vacuna suministrada';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            action(ALVAsignarVacunaBloqueadaC01)
            {
                Caption = '💉 Asignar vacuna bloqueada';
                ApplicationArea = All;

                trigger OnAction()
                begin
                    Rec.Validate(ALVTipoVacunaC01, 'BLOQUEADO');
                end;
            }
            action(ALVSwitchVariableC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba variables globales a BC';

                trigger OnAction()
                begin
                    Message('El valor actual es %1. Lo cambiamos a %2', cuALVFuncionesAppC01.SwitchF(), not cuALVFuncionesAppC01.SwitchF());
                    cuALVFuncionesAppC01.SwitchF(not cuALVFuncionesAppC01.SwitchF());
                end;
            }
            action(ALVCrearProveedorC01)
            {
                Caption = 'Crear cliente como proveedor';
                ApplicationArea = All;
                Image = Customer;

                trigger OnAction()
                var
                    rlVendor: Record Vendor;
                begin
                    if Rec.Get(rlVendor.Name) then begin
                        Message('El cliente ya existe');
                    end else begin
                        rlVendor.Init();
                        rlVendor.TransferFields(Rec, true, true);
                        rlVendor.Insert();
                        Message('Cliente creado correctamente');
                    end;
                end;
            }
        }
    }

    var
        cuALVFuncionesAppC01: Codeunit ALVVariablesGlobalesAppC01;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('¿Seguro que desea salir de la página?', false));
    end;
}
