page 50109 "ALVEjDiccionarioC01"
{
    ApplicationArea = All;
    Caption = 'Ejemplo diccionario';
    PageType = List;
    SourceTable = Integer; // Es una tabla virtual (Hay otra que se llama Date, y otra que se llama File)
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Num; Rec.Number)
                {
                    ApplicationArea = All;
                    Caption = 'Nº';
                }
                field(Campo01; ValorCeldF(Rec.Number, 1))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 01';
                }
                field(Campo02; ValorCeldF(Rec.Number, 2))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 02';
                }
                field(Campo03; ValorCeldF(Rec.Number, 3))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 03';
                }
                field(Campo04; ValorCeldF(Rec.Number, 4))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 04';
                }
                field(Campo05; ValorCeldF(Rec.Number, 5))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 05';
                }
                field(Campo06; ValorCeldF(Rec.Number, 6))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 06';
                }
                field(Campo07; ValorCeldF(Rec.Number, 7))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 07';
                }
                field(Campo08; ValorCeldF(Rec.Number, 8))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 08';
                }
                field(Campo09; ValorCeldF(Rec.Number, 9))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 09';
                }
                field(Campo10; ValorCeldF(Rec.Number, 10))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 10';
                }
            }
        }
    }

    local procedure ValorCeldF(pFila: Integer; pColumna: Integer): Decimal
    begin
        // Devuelve el get con pFila y pColumna
        exit(xDiccionario.Get(pFila).Get(pColumna));
    end;

    trigger OnOpenPage()
    var
        xlFila: Integer;
        xlColumna: Integer;
        xlNumeros: List of [Decimal];
    begin
        for xlfila := 1 to 10 do begin
            Clear(xlNumeros);
            for xlColumna := 1 to 10 do begin
                xlNumeros.Add(Random(555555));
            end;
            xDiccionario.Add(xlFila, xlNumeros);
        end;
        Rec.SetRange(Number, 1, 10);
        EjemploNotificacionF();
    end;

    local procedure EjemploNotificacionF()
    var
        xlNotificacion: Notification;
        xlTexto: Label 'Carga %1 finalizada';
        i: Integer;
    begin
        for i := 1 to 3 do begin
            xlNotificacion.Id(CreateGuid());
            xlNotificacion.Message(StrSubstNo(xlTexto, i));
            xlNotificacion.Send();
        end;
        xlNotificacion.Id(CreateGuid());
        xlNotificacion.Message('El cliente no tiene ninguna vacuna');
        xlNotificacion.AddAction('Abrir lista de clientes', Codeunit::ALVFuncionesAppC01, 'AbrirListaClientesF');
        xlNotificacion.SetData('CodigoCliente', '10000');
        xlNotificacion.Send();
    end;

    var
        xDiccionario: Dictionary of [Integer, List of [Decimal]];
}
