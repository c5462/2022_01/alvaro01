codeunit 50102 "ALVCapturarErroresC01"
{
    trigger OnRun()
    var
        rlCustomer: Record Customer;
        clCodCte: Label 'ABC', Locked = true, Comment = 'Comentario programacion';
    begin
        if not rlCustomer.Get(clCodCte) then begin
            rlCustomer.Init();
            rlCustomer.Validate("No.", clCodCte);
            rlCustomer.Insert(true); //Ponemos true para que ejecute el trigger de OnInsert
            rlCustomer.Validate(Name, 'Cliente prueba');
            rlCustomer.Validate(Address, 'Mi calle');
            rlCustomer."VAT Registration No." := 'xxxxx';
            rlCustomer.Validate("Payment Method Code", 'ALV');
            rlCustomer.Modify(true);
        end;
        Message('Datos del cliente: %1', rlCustomer);
    end;

    procedure CapturarErrorF(pCodProveedor: Code[20]): Boolean;
    var
        rlVendor: Record Vendor;
    begin
        if not rlVendor.Get(pCodProveedor) then begin
            rlVendor.Init();
            rlVendor.Validate("No.", pCodProveedor);
            rlVendor.Insert(true); //Ponemos true para que ejecute el trigger de OnInsert
            if AsignarDatosProveedorF(rlVendor) then begin
                rlVendor.Modify(true);
            end else begin
                Message('Error al asignar datos (%1)', GetLastErrorText());
            end;
            exit(true);
        end;
        Message('Ya existe el proveedor %1', pCodProveedor);
    end;

    [TryFunction]
    procedure AsignarDatosProveedorF(rlVendor: Record Vendor)
    begin
        rlVendor.Validate(Name, 'Proveedor prueba');
        rlVendor.Validate(Address, 'Mi calle');
        rlVendor."VAT Registration No." := 'xxxxx';
        rlVendor.Validate("Payment Method Code", 'ALV');
    end;

    [TryFunction]
    procedure AsignarDatosPedidosVentaF(var pSalesHeader: Record "Sales Header"; pLinea: Text; pPosition: Integer; pSeparator: Text)
    var
        xlFecha: Date;
        xlEstado: Enum "Sales Document Status";
    begin
        case pPosition of
            4:
                begin
                    pSalesHeader.Validate("Bill-to Name", pLinea.Split(pSeparator).Get(4));
                end;
            5:
                begin
                    pSalesHeader.Validate("Bill-to Contact", pLinea.Split(pSeparator).Get(5));
                end;
            6:
                begin
                    pSalesHeader.Validate("Bill-to Customer No.", pLinea.Split(pSeparator).Get(6));
                end;
            7:
                begin
                    pSalesHeader.Validate("Sell-to Customer No.", pLinea.Split(pSeparator).Get(7));
                end;
            8:
                begin
                    if Evaluate(xlEstado, pLinea.Split(pSeparator).Get(8)) then begin
                        pSalesHeader.Validate("Status", xlEstado);
                    end;
                end;
            9:
                begin
                    if Evaluate(xlFecha, pLinea.Split(pSeparator).Get(9)) then begin
                        pSalesHeader.Validate("Posting Date", xlFecha);
                    end;
                end;
            10:
                begin
                    if Evaluate(xlFecha, pLinea.Split(pSeparator).Get(10)) then begin
                        pSalesHeader.Validate("Due Date", xlFecha);
                    end;
                end;
            11:
                begin
                    if Evaluate(xlFecha, pLinea.Split(pSeparator).Get(11)) then begin
                        pSalesHeader.Validate("Shipment Date", xlFecha);
                    end;
                end;
        end;
    end;

    [TryFunction]
    procedure AsignarDatosPedidosVentaF(var pSalesLine: Record "Sales Line"; pLinea: Text; pPosition: Integer; pSeparator: Text)
    var
        xlType: Enum "Sales Line Type";
        xlAmount: Decimal;
    begin
        case pPosition of
            5:
                begin
                    if Evaluate(xlType, pLinea.Split(pSeparator).Get(5)) then begin
                        pSalesLine.Validate(Type, xlType);
                    end;
                end;
            6:
                begin
                    pSalesLine.Validate("No.", pLinea.Split(pSeparator).Get(6));
                end;
            7:
                begin
                    if Evaluate(xlAmount, pLinea.Split(pSeparator).Get(7)) then begin
                        pSalesLine.Validate(Quantity, xlAmount);
                    end;
                end;
            8:
                begin
                    if Evaluate(xlAmount, pLinea.Split(pSeparator).Get(8)) then begin
                        pSalesLine.Validate("Unit Price", xlAmount);
                    end;
                end;
            9:
                begin
                    pSalesLine.Validate(Description, pLinea.Split(pSeparator).Get(9));
                end;
            10:
                begin
                    if Evaluate(xlAmount, pLinea.Split(pSeparator).Get(10)) then begin
                        pSalesLine.Validate(Amount, xlAmount);
                    end;
                end;
        end;
    end;
}