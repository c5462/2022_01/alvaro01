/*/// <summary>
/// PageExtension ALVCompaniesListExtC01 (ID 50106) extends Record Companies.
/// </summary>
pageextension 50106 "ALVCompaniesListExtC01" extends Companies
{
    layout
    {
        addlast(Control1)
        {
            field(ALVIdC01; Rec.Id)
            {
                Caption = 'Id';
                ApplicationArea = All;
            }
            field(ALVNombreEmpresaC01; Rec.Name)
            {
                Caption = 'Empresa';
                ApplicationArea = All;
            }
            field("ALVNum_ClientesC01"; Num_Clientes)
            {
                Caption = 'Nº Clientes';
                ApplicationArea = All;
            }
            field("ALVNum_CuentasC01"; Num_Cuentas)
            {
                Caption = 'Nº Cuentas';
                ApplicationArea = All;
            }
            field("ALVNum_ProductosC01"; Num_Productos)
            {
                Caption = 'Nº Productos';
                ApplicationArea = All;
            }
            field("ALVNum_ProveedoresC01"; Num_Proveedores)
            {
                Caption = 'Nº Proveedores';
                ApplicationArea = All;
            }
        }
    }

    trigger OnOpenPage()
    begin
        rlEmpresas.TransferFields(rlCompanies);
        if rlEmpresas.FindSet(false) then begin
            repeat
                if rlEmpresas.CurrentCompany.Contains(rlEmpresas.NombreEmpresa) then begin
                    Rec.Init();
                    Rec.Validate(Id, rlEmpresas.Id);
                    Rec.Validate(Name, rlEmpresas.NombreEmpresa);
                    Rec.Validate(Num_Clientes, rlClientes.Count());
                    Rec.Validate(Num_Cuentas, rlCuentas.Count());
                    Rec.Validate(Num_Proveedores, rlProveedores.Count());
                    Rec.Validate(Num_Productos, rlProductos.Count());
                    Rec.Insert(true);
                end else begin
                    Rec.ChangeCompany(rlEmpresas.NombreEmpresa);
                end;
            until rlEmpresas.Next() = 0;
        end else begin
            Message('Proceso erroneo');
        end;
        rlEmpresas.Find();
    end;

    var
        Num_Clientes: Integer;
        Num_Cuentas: Integer;
        Num_Productos: Integer;
        Num_Proveedores: Integer;
        rlEmpresas: Record ALVEmpresasC01;
        rlCompanies: Record Company;
        rlCuentas: Record "G/L Account";
        rlClientes: Record Customer;
        rlProveedores: Record Vendor;
        rlProductos: Record Item;
}
*/