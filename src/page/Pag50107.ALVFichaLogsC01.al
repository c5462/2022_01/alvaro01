page 50107 "ALVFichaLogsC01"
{
    Caption = 'Ficha Logs';
    PageType = List;
    ApplicationArea = all;
    UsageCategory = Lists;
    SourceTable = ALVLogC01;
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = true;
    SourceTableView = sorting(SystemCreatedAt) order(descending);

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Id; Rec.Id)
                {
                    Caption = 'Id';
                    ToolTip = 'Especifica el id del log';
                    ApplicationArea = All;
                }
                field(Mensaje; Rec.Mensaje)
                {
                    Caption = 'Mensaje';
                    ToolTip = 'Especifica el mensaje de cada log';
                    ApplicationArea = All;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    Caption = 'CreateAt';
                    ToolTip = 'Especifica el valor de SystemCreatedAt';
                    ApplicationArea = All;
                }
            }
        }
    }

    /*
    //Otra forma de ordenar una lista a partir de un campo es con un disparador
    trigger OnOpenPage()
    begin
        Rec.SetCurrentKey(SystemCreatedAt);
        Rec.SetAscending(SystemCreatedAt, false);
    end;
    */
}
