page 50104 "ALVFichaCabPlanVacunacionC01"
{
    Caption = 'Ficha Cabecera Plan Vacunacion';
    PageType = Document;
    SourceTable = ALVCabPlanVacunacionC01;
    ApplicationArea = All;
    UsageCategory = Documents;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(FechaVacunacionPlanificada; Rec.FechaVacunacionPlanificada)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunacion Planificada field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre empresa vacunadora';
                    ApplicationArea = All;
                    ToolTip = 'Aquí aparecerá el nombre del proveedor que realizará la vacunación';
                }
            }
            part(Lineas; ALVSubPageLinPlanVacunacionC01)
            {
                ApplicationArea = all;
                SubPageLink = CodigoLineas = field(CodigoCabecera);
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(EjemploEntradaSalida)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de variables 01';

                trigger OnAction()
                var
                    culALVFuncionesAppC01: Codeunit ALVFuncionesAppC01;
                    xlTexto: Text;
                begin
                    xlTexto := 'Alvaro servicios';
                    culALVFuncionesAppC01.EjemploVariablesEntradaSalidaF(xlTexto);
                    Message(xlTexto);
                end;
            }
            action(EjemploEntradaSalida02)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de variables 01';

                trigger OnAction()
                var
                    culALVFuncionesAppC01: Codeunit ALVFuncionesAppC01;
                    xlTexto: TextBuilder;
                begin
                    xlTexto.Append('Alvaro servicios');
                    culALVFuncionesAppC01.EjemploObjetosEntradaSalida02F(xlTexto);
                    Message(xlTexto.ToText());
                end;
            }

            action(Bingo)
            {
                ApplicationArea = all;
                Caption = 'BINGO';

                trigger OnAction()
                var
                    culALVFuncionesAppC01: Codeunit ALVFuncionesAppC01;
                begin
                    culALVFuncionesAppC01.EjemploBingoF();
                end;
            }

            // Ejecutar la acción en segundo plano
            action(PruebaFncSegPlano)
            {
                ApplicationArea = all;
                Caption = 'Prueba función en seguno plano';

                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    StartSession(xlSesionIniciada, Codeunit::ALVCodeUnitLentaC01);
                    Message('Proceso lanzado en segundo plano');
                end;
            }

            // Ejecuta la función en primer plano
            action(PruebaFncPrimerPlano)
            {
                ApplicationArea = all;
                Caption = 'Prueba función en primer plano';

                trigger OnAction()
                var
                    culALVPrimerPlano: Codeunit ALVCodeUnitLentaC01;
                begin
                    culALVPrimerPlano.Run();
                    Message('Proceso lanzado en primer plano');
                end;
            }

            action(Logs)
            {
                ApplicationArea = all;
                // Con Windows + punto podemos añadir imagenes al caption
                Caption = '📝 Logs';
                // Con image tambien podemos añadir imagenes
                Image = Log;
                RunObject = page ALVFichaLogsC01;
            }

            action(CrearLineas)
            {
                ApplicationArea = all;
                Caption = 'Crear líneas bulk';
                Image = Create;

                trigger OnAction()
                var
                    rlLineasPlanVacunacion: Record ALVLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;

                    for i := 1 to 10000 do begin
                        rlLineasPlanVacunacion.Init();
                        rlLineasPlanVacunacion.Validate(CodigoLineas, rec.CodigoCabecera);
                        rlLineasPlanVacunacion.Validate(N_Linea, i);
                        rlLineasPlanVacunacion.Insert(true);
                    end;
                    Message('Ha tardado %1', CurrentDateTime - xlInicio);
                end;
            }

            action(CrearLineasNoBulk)
            {
                ApplicationArea = all;
                Caption = 'Crear líneas no bulk';
                Image = Create;

                trigger OnAction()
                var
                    rlLineasPlanVacunacion: Record ALVLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;

                    for i := 1 to 10000 do begin
                        rlLineasPlanVacunacion.Init();
                        rlLineasPlanVacunacion.Validate(CodigoLineas, rec.CodigoCabecera);
                        rlLineasPlanVacunacion.Validate(N_Linea, i);
                        if not rlLineasPlanVacunacion.Insert(true) then begin
                            Error('No se ha podido insertar las lineas'); //El if ralentiza el proceso de inserción
                        end;
                    end;
                    Message('Ha tardado %1', CurrentDateTime - xlInicio);
                end;
            }

            //TARDA MUCHO MENOS QUE EL MODIFY()
            action(ModificarLineas)
            {
                ApplicationArea = all;
                Caption = 'Modificar líneas bulk';
                Image = Create;

                trigger OnAction()
                var
                    rlLineasPlanVacunacion: Record ALVLinPlanVacunacionC01;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlLineasPlanVacunacion.SetRange(CodigoLineas, rec.CodigoCabecera);
                    rlLineasPlanVacunacion.ModifyAll(FechaVacuna, Today, true);
                    Message('Ha tardado %1', CurrentDateTime - xlInicio);
                end;
            }

            //TARDA MUCHO MÁS QUE EL MODIFYALL()
            action(ModificarLineasNoBulk)
            {
                ApplicationArea = all;
                Caption = 'Modificar líneas no bulk';
                Image = Create;

                trigger OnAction()
                var
                    rlLineasPlanVacunacion: Record ALVLinPlanVacunacionC01;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlLineasPlanVacunacion.SetRange(CodigoLineas, rec.CodigoCabecera);
                    if rlLineasPlanVacunacion.FindSet(true, false) then begin
                        repeat
                            rlLineasPlanVacunacion.Validate(FechaVacuna, Today + 1);
                            rlLineasPlanVacunacion.Modify(true);
                        until rlLineasPlanVacunacion.Next() = 0;
                    end;

                    Message('Ha tardado %1', CurrentDateTime - xlInicio);
                end;
            }
        }
    }
}
