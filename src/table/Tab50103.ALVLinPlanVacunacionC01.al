table 50103 "ALVLinPlanVacunacionC01"
{
    Caption = 'Líneas Plan De Vacunación';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; CodigoLineas; Code[20])
        {
            Caption = 'Código';
            DataClassification = SystemMetadata;
        }
        field(2; "N_Linea"; Integer)
        {
            Caption = 'Nº Línea';
            DataClassification = SystemMetadata;
        }
        field(3; Cliente; Code[20])
        {
            Caption = 'Cliente';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
            trigger OnValidate()
            var
                rlALVCabeceraC01: Record ALVCabPlanVacunacionC01;
            begin
                if Rec.FechaVacuna = 0D then begin
                    if rlALVCabeceraC01.Get(Rec.CodigoLineas) then begin
                        Rec.Validate(FechaVacuna, rlALVCabeceraC01.FechaVacunacionPlanificada);
                    end;
                end;
            end;
        }
        field(4; CodigoVacuna; Code[20])
        {
            Caption = 'Tipo Vacuna';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = ALVVariosC01.Codigo where(Tipo = const(Vacuna), Bloqueado = const(false));
            trigger OnValidate()
            begin
                CalculaFechaF();
            end;
        }
        field(5; CodigoOtros; Code[20])
        {
            Caption = 'Tipo Otros';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = ALVVariosC01.Codigo where(Tipo = const(Otros), Bloqueado = const(false));
        }
        field(6; FechaVacuna; Date)
        {
            Caption = 'Fecha Vacuna';
            DataClassification = OrganizationIdentifiableInformation;
            trigger OnValidate()
            begin
                CalculaFechaF();
            end;
        }
        field(7; FechaSegundaVacuna; Date)
        {
            Caption = 'Fecha 2ª Vacuna';
            DataClassification = OrganizationIdentifiableInformation;
        }
    }
    keys
    {
        key(PK; CodigoLineas, N_Linea)
        {
            Clustered = true;
        }
    }

    procedure NombreClienteF(): Text
    var
        rlCliente: Record Customer; //Variable local de tipo Registro (rlXXX)
    begin
        if rlCliente.Get(Rec.Cliente) then begin
            exit(rlCliente.Name);
        end;
    end;

    procedure DescripcionVariosF(pTipo: Enum ALVTipoC01; pCode: Code[20]): Text
    var
        rlVarios: Record ALVVariosC01;
    begin
        if rlVarios.Get(pTipo, pCode) then begin
            exit(rlVarios.Descripcion);
        end;
    end;

    procedure CalculaFechaF()
    var
        rlALVVariosC01: Record ALVVariosC01;
        IsHandled: Boolean;
    begin
        OnBeforeCalcularFechaProxVacunaF(Rec, xRec, rlALVVariosC01, IsHandled, CurrFieldNo);
        if not IsHandled then begin
            if Rec.FechaVacuna <> 0D then begin
                if rlALVVariosC01.Get(rlALVVariosC01.Tipo::Vacuna, rec.CodigoVacuna) then begin
                    Rec.Validate(FechaSegundaVacuna, CalcDate(rlALVVariosC01.FechaSegundaVac, FechaVacuna));
                end;
            end;
        end;
        OnAfterCalcularFechaProxVacunaF(Rec, xRec, rlALVVariosC01, CurrFieldNo);
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterCalcularFechaProxVacunaF(Rec: Record ALVLinPlanVacunacionC01; xRec: Record ALVLinPlanVacunacionC01; rlALVVariosC01: Record ALVVariosC01; CurrFieldNo: Integer)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeCalcularFechaProxVacunaF(Rec: Record ALVLinPlanVacunacionC01; xRec: Record ALVLinPlanVacunacionC01; rlALVVariosC01: Record ALVVariosC01; var IsHandled: Boolean; CurrFieldNo: Integer)
    begin
    end;
}