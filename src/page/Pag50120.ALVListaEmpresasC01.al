page 50120 "ALVListaEmpresasC01"
{
    ApplicationArea = All;
    Caption = 'Lista de Empresas';
    PageType = List;
    SourceTable = ALVEmpresasC01;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(IdEmpresa; Rec.Id)
                {
                    Caption = 'Id';
                    ApplicationArea = All;
                }
                field(NombreEmpresa; Rec.NombreEmpresa)
                {
                    Caption = 'Empresa';
                    ApplicationArea = All;
                }
                field("Num_Clientes"; Rec.Num_Clientes)
                {
                    Caption = 'Nº Clientes';
                    ApplicationArea = All;
                }
                field("Num_Cuentas"; Rec.Num_Cuentas)
                {
                    Caption = 'Nº Cuentas';
                    ApplicationArea = All;
                }
                field("Num_Productos"; Rec.Num_Productos)
                {
                    Caption = 'Nº Productos';
                    ApplicationArea = All;
                }
                field("Num_Proveedores"; Rec.Num_Proveedores)
                {
                    Caption = 'Nº Proveedores';
                    ApplicationArea = All;
                }
            }
        }
    }

    trigger OnAfterGetRecord()
    var
        rlCompanies: Record Company;
        rlCuentas: Record "G/L Account";
        rlClientes: Record Customer;
        rlProveedores: Record Vendor;
        rlProductos: Record Item;
    begin
        if rlCompanies.FindSet(false) then begin
            repeat
                if Rec.Id <> CompanyName then begin
                    Rec.Init();
                    Rec.Validate(Id, rlCompanies.Name);
                    Rec.Insert(true);
                    Rec.Validate(NombreEmpresa, rlCompanies."Display Name");
                    Rec.Validate(Num_Clientes, rlClientes.Count());
                    Rec.Validate(Num_Cuentas, rlCuentas.Count());
                    Rec.Validate(Num_Proveedores, rlProveedores.Count());
                    Rec.Validate(Num_Productos, rlProductos.Count());
                    Rec.Modify(true);
                end else begin
                    Rec.ChangeCompany(rlCompanies.Name);
                end;
            until rlCompanies.Next() = 0;
        end;
        rlCompanies.Find();
    end;
}