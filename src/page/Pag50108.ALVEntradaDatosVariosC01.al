/// <summary>
/// Page MAAEntradaDatosVariosC01 (ID 55108). Ejercicio de página comodín. 
/// Para pasar varios datos y que me devuelve varios datos. 
/// </summary>
page 50108 "ALVEntradaDatosVariosC01"
{
    Caption = 'Entrada de Datos Varios';
    PageType = StandardDialog;

    layout
    {
        area(content)
        {
            field(Texto01; aTexto[1])
            {
                ApplicationArea = All;
                Visible = aIsTxtVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 1];
            }

            field(Texto02; aTexto[2])
            {
                ApplicationArea = All;
                Visible = aIsTxtVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 2];
            }

            field(Texto03; aTexto[3])
            {
                ApplicationArea = All;
                Visible = aIsTxtVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 3];
            }
            field(Texto04; aTexto[4])
            {
                ApplicationArea = All;
                Visible = aIsTxtVisible04;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 4];
            }
            field(Bool01; aBools[1])
            {
                ApplicationArea = All;
                Visible = aIsBoolVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 1];
            }
            field(Bool02; aBools[2])
            {
                ApplicationArea = All;
                Visible = aIsBoolVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 2];
            }
            field(Bool03; aBools[3])
            {
                ApplicationArea = All;
                Visible = aIsBoolVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 3];
            }
            field(Decimal01; aDecimal[1])
            {
                ApplicationArea = All;
                Visible = aIsDecVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 1];
            }
            field(Decimal02; aDecimal[2])
            {
                ApplicationArea = All;
                Visible = aIsDecVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 2];
            }
            field(Decimal03; aDecimal[3])
            {
                ApplicationArea = All;
                Visible = aIsDecVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 3];
            }
            field(Fecha01; aDate[1])
            {
                ApplicationArea = All;
                Visible = aIsDateVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 1];
            }
            field(Fecha02; aDate[2])
            {
                ApplicationArea = All;
                Visible = aIsDateVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 2];
            }
            field(Fecha03; aDate[2])
            {
                ApplicationArea = All;
                Visible = aIsDateVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 3];
            }
            field(Contra01; aContra[1])
            {
                ApplicationArea = All;
                Visible = aIsContraVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Password, 1];
                ExtendedDatatype = Masked;
            }
            field(Contra02; aContra[2])
            {
                ApplicationArea = All;
                Visible = aIsContraVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Password, 2];
                ExtendedDatatype = Masked;
            }
            field(Contra03; aContra[3])
            {
                ApplicationArea = All;
                Visible = aIsContraVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Password, 3];
                ExtendedDatatype = Masked;
            }
        }
    }
    var
        // Variable para definir el tipo de objeto
        xTipoDato: Option Nulo,Texto,Booleano,Numerico,Fecha,Password;
        xTipoPuntero: Option Nulo,Pasado,Leido;

        // Variables para hacer los datos visibles o no
        aIsTxtVisible01: Boolean;
        aIsTxtVisible02: Boolean;
        aIsTxtVisible03: Boolean;
        aIsTxtVisible04: Boolean;
        aIsBoolVisible01: Boolean;
        aIsBoolVisible02: Boolean;
        aIsBoolVisible03: Boolean;
        aIsDecVisible01: Boolean;
        aIsDecVisible02: Boolean;
        aIsDecVisible03: Boolean;
        aIsDateVisible01: Boolean;
        aIsDateVisible02: Boolean;
        aIsDateVisible03: Boolean;
        aIsContraVisible01: Boolean;
        aIsContraVisible02: Boolean;
        aIsContraVisible03: Boolean;

        // Variables para mostrar
        mPunteros: array[2, 5] of Integer;
        mCaptionClass: array[5, 10] of Text;
        aTexto: array[4] of Text; //Muy importante!! Los arrays empiezan en el [1] no en el [0]
        aBools: array[3] of Boolean;
        aDecimal: array[3] of Decimal;
        aDate: array[3] of Date;
        aContra: array[3] of Text;


    /// <summary>
    /// CampoF.
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] += 1;
        mCaptionClass[xTipoDato::Texto, mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pCaption;
        aTexto[mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pValorInicial;
        aIsTxtVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 1;
        aIsTxtVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 2;
        aIsTxtVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 3;
        aIsTxtVisible04 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 4;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo Booleano, pasando el caption y el valor del campo
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Boolean.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Boolean)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] += 1;
        mCaptionClass[xTipoDato::Booleano, mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pCaption;
        aBools[mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pValorInicial;
        aIsBoolVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 1;
        aIsBoolVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 2;
        aIsBoolVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 3;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo Decimal, pasando el caption y el valor del campo
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Decimal.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Decimal)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] += 1;
        mCaptionClass[xTipoDato::Numerico, mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pCaption;
        aDecimal[mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pValorInicial;
        aIsDecVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 1;
        aIsDecVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 2;
        aIsDecVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 3;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo Fecha, pasando el caption y el valor del campo
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Fecha.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Date)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] += 1;
        mCaptionClass[xTipoDato::Fecha, mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pCaption;
        aDate[mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pValorInicial;
        aIsDateVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 1;
        aIsDateVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 2;
        aIsDateVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 3;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo Fecha, pasando el caption y el valor del campo
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    /// <param name="pValorNoUsado">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text; esPassword: Boolean)
    begin
        if esPassword then begin
            mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] += 1;
            mCaptionClass[xTipoDato::Password, mPunteros[xTipoPuntero::Pasado, xTipoDato::Password]] := pCaption;
            aContra[mPunteros[xTipoPuntero::Pasado, xTipoDato::Password]] := pValorInicial;
            aIsContraVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 1;
            aIsContraVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 2;
            aIsContraVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 3;
        end else begin
            // Si no es password que lo analice como un texto
            CampoF(pCaption, pValorInicial);
        end;
    end;

    /// <summary>
    /// Devuelve el texto introducido por el usuario.
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(): Text
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Texto] += 1;
        exit(aTexto[mPunteros[xTipoPuntero::Leido, xTipoDato::Texto]]); //Lo puede devolver porque es variable global
    end;

    /// <summary>
    /// CampoF.
    /// </summary>
    /// <param name="pSalida">VAR Boolean.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure CampoF(var pSalida: Boolean): Boolean
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano] += 1;
        pSalida := aBools[mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano]]; //Lo puede devolver porque es variable global
        exit(pSalida);
    end;

    /// <summary>
    /// CampoF.
    /// </summary>
    /// <param name="pSalida">VAR Decimal.</param>
    /// <returns>Return value of type Decimal.</returns>
    procedure CampoF(var pSalida: Decimal): Decimal
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Numerico] += 1;
        pSalida := aDecimal[mPunteros[xTipoPuntero::Leido, xTipoDato::Numerico]]; //Lo puede devolver porque es variable global
        exit(pSalida);
    end;

    /// <summary>
    /// CampoF.
    /// </summary>
    /// <param name="pSalida">VAR Date.</param>
    /// <returns>Return value of type Date.</returns>
    procedure CampoF(var pSalida: Date): Date
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha] += 1;
        pSalida := aDate[mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha]]; //Lo puede devolver porque es variable global
        exit(pSalida);
    end;

    /// <summary>
    /// CampoF.
    /// </summary>
    /// <param name="pSalida">VAR Text.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Text): Text
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Password] += 1;
        pSalida := aContra[mPunteros[xTipoPuntero::Leido, xTipoDato::Password]]; //Lo puede devolver porque es variable global
        exit(pSalida);
    end;

    /// <summary>
    /// Devuelve el texto introducido por el usuario.
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(pPuntero: Integer): Text
    begin
        exit(aTexto[pPuntero]); //Lo puede devolver porque es variable global
    end;

    /// <summary>
    /// CampoF.
    /// </summary>
    /// <param name="pSalida">VAR Boolean.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure CampoF(var pSalida: Boolean; pPuntero: Integer): Boolean
    begin
        pSalida := aBools[pPuntero]; //Lo puede devolver porque es variable global
        exit(pSalida);
    end;

    /// <summary>
    /// CampoF.
    /// </summary>
    /// <param name="pSalida">VAR Decimal.</param>
    /// <returns>Return value of type Decimal.</returns>
    procedure CampoF(var pSalida: Decimal; pPuntero: Integer): Decimal
    begin
        pSalida := aDecimal[pPuntero]; //Lo puede devolver porque es variable global
        exit(pSalida);
    end;

    /// <summary>
    /// CampoF.
    /// </summary>
    /// <param name="pSalida">VAR Date.</param>
    /// <returns>Return value of type Date.</returns>
    procedure CampoF(var pSalida: Date; pPuntero: Integer): Date
    begin
        pSalida := aDate[pPuntero]; //Lo puede devolver porque es variable global
        exit(pSalida);
    end;

    /// <summary>
    /// CampoF.
    /// </summary>
    /// <param name="pSalida">VAR Text.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Text; pPuntero: Integer): Text
    begin
        pSalida := aContra[pPuntero]; //Lo puede devolver porque es variable global
        exit(pSalida);
    end;
}
