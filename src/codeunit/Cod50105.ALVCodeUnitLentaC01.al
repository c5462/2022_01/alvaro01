codeunit 50105 "ALVCodeUnitLentaC01"
{
    trigger OnRun()
    begin
        EjecutarTareaF();
    end;


    // Siempre que hagamos cosas de ventanas de dialogo y configuracion hay que poner "if GuiAllowed"
    // Para comprobar si el cliente tiene interfaz de usuario
    local procedure EjecutarTareaF()
    var
        xlEjecutar: Boolean;
    begin
        xlEjecutar := true;
        if GuiAllowed then begin
            xlEjecutar := Confirm('¿Desea ejecutar esta tarea? Podría demorar varios minutos');
        end;
        if xlEjecutar then begin
            InsertarLog('Tarea iniciada');
            Commit();   //No usar nunca
            Message('En proceso');
            Sleep(10000);
            InsertarLog('Tarea finalizada');
        end;
    end;

    local procedure InsertarLog(pMensaje: Text)
    var
        rlLog: Record ALVLogC01;
    begin
        rlLog.Init();
        rlLog.Insert(true);
        rlLog.Validate(Mensaje, pMensaje);
        rlLog.Modify(true);
    end;
}
