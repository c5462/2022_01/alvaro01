page 50100 "ALVConfiguracionC01"
{
    Caption = 'Configuración de la App 01 del Curso';
    PageType = Card;
    SourceTable = ALVConfiguracionC01;
    UsageCategory = Administration;
    ApplicationArea = All;
    AboutTitle = 'En esta página se crea la configuración para la primera App del curso Esp. en Business Central';
    AboutText = 'Configure la App de forma correcta. Asegurese de asignar el valor de campo Cliente Web';
    AdditionalSearchTerms = 'Mi primera App';
    //AutoSplitKey = true;
    DeleteAllowed = false; //Importante!! para las páginas de configuración
    InsertAllowed = false; //No permite insertar registros (para pag. de config.)
    ShowFilter = true;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(CodClienteWeb; Rec.CodClienteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por Web';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Cod. Cliente Web';
                    AboutText = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por Web';
                    Importance = Promoted; // Te promueve arriba de la pestaña el campo
                }
                field(TextoReg; Rec.TextoReg)
                {
                    ToolTip = 'Specifies the value of the Texto Registro field.';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Texto Registro';
                    AboutText = 'Especifique el texto del registro del cliente de BC';
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo de Documento field.';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tabla Condicional field.';
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the CodTabla field.';
                    ApplicationArea = All;
                }
                field(NombreTabla; Rec.NombreTablaSelecF(Rec.TipoTabla, Rec.CodTabla))
                {
                    Caption = 'Nombre Tabla Seleccionada';
                    ApplicationArea = All;
                    Editable = false; //Aunque las funciones nunca serán editables
                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    Caption = 'Color de Fondo';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    Caption = 'Color de Letra';
                    ApplicationArea = All;
                }
                field(xPassword; Rec.IdPassword)
                {
                    Caption = 'Password';
                    ApplicationArea = All;
                }
                field(NumIteraciones; xNumIteraciones)
                {
                    Caption = 'Nº de iteraciones';
                    ApplicationArea = all;
                }

            }
            group(InfInterna)
            {
                Caption = 'Información Interna';
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Importance = Additional; // Muestra el campo cuando pulsamos mostrar más
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
            group(CamposCalculados)
            {
                Caption = 'Campos Calculados';
                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Specifies the value of the NombreCliente field.';
                    ApplicationArea = All;
                }
                field(NombreClienteF; Rec.NombreClienteF(Rec.CodClienteWeb))
                {
                    Caption = 'Nombre del Cliente (función)';
                    ApplicationArea = All;
                }
                field(Stock; Rec.Stock)
                {
                    Caption = 'Stock';
                    ToolTip = 'Especifica el stock disponible';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Ejemplo01)
            {
                ApplicationArea = All;
                Caption = 'Mensaje de Acción';
                Image = AboutNav;
                trigger OnAction()
                begin
                    Message('Acción de ejemplo pulsada');
                end;
            }
            //Botón que ejecutará mi página MAAEntradaDatosVariosC01
            action(PedirDatos)
            {
                ApplicationArea = All;
                Caption = 'Pedir Datos Varios';

                trigger OnAction()
                var
                    pglALVEntradaDatosVariosC01: Page ALVEntradaDatosVariosC01;
                    xlBool: Boolean;
                    xlDecimal: Decimal;
                    xlFecha: Date;
                begin
                    pglALVEntradaDatosVariosC01.CampoF('Ciudad', 'Albacete');
                    pglALVEntradaDatosVariosC01.CampoF('Pais de nacimiento', 'España');
                    pglALVEntradaDatosVariosC01.CampoF('Codigo Postal', '02001');
                    pglALVEntradaDatosVariosC01.CampoF('Madre', 'Carolina');
                    pglALVEntradaDatosVariosC01.CampoF('Bool1', true);
                    pglALVEntradaDatosVariosC01.CampoF('Bool2', false);
                    pglALVEntradaDatosVariosC01.CampoF('Bool3', true);
                    pglALVEntradaDatosVariosC01.CampoF('Decimal1', 2.2);
                    pglALVEntradaDatosVariosC01.CampoF('Decimal2', 2.1);
                    pglALVEntradaDatosVariosC01.CampoF('Decimal3', 2.9);
                    pglALVEntradaDatosVariosC01.CampoF('Date', 0D);
                    pglALVEntradaDatosVariosC01.CampoF('Date', 0D);
                    pglALVEntradaDatosVariosC01.CampoF('Date', 0D);
                    //pglMAAEntradaDatosVariosC01.Run(); //Si uso esta solo puedo ejecutar, no puedo recoger parámetros, hay que usar la siguiente
                    if pglALVEntradaDatosVariosC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF());
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF());
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF());
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF());
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF(xlBool));
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF(xlBool));
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF(xlBool));
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF(xlDecimal));
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF(xlDecimal));
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF(xlDecimal));
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF(xlFecha));
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF(xlFecha));
                        Message('Valor introducido: %1', pglALVEntradaDatosVariosC01.CampoF(xlFecha));
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }

            action(CambiarContraseña)
            {
                ApplicationArea = All;
                Caption = 'Cambiar contraseña';

                trigger OnAction()
                begin
                    cambiarPasswordF();
                end;
            }

            action(DescargarVacuna)
            {
                ApplicationArea = All;
                Caption = 'Exportar vacunas';

                trigger OnAction()
                begin
                    ExportarVacunaF();
                end;
            }

            action(SubirVacuna)
            {
                ApplicationArea = All;
                Caption = 'Importar vacunas';

                trigger OnAction()
                begin
                    ImportarVacunaF();
                end;
            }

            action(MostrarClientesGris)
            {
                ApplicationArea = All;
                Caption = 'Mostrar clientes almacén gris de una Empresa';

                trigger OnAction()
                begin
                    MostrarClientesGrisF();
                end;
            }
            action(ALVPruebaTxtC01)
            {
                ApplicationArea = all;
                Caption = 'Pruebas con TXT';
                Image = Text;

                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: Text;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumIteraciones do begin
                        xlTxt += '.';
                    end;
                    Message('Tiempo invertido %1 %2', CurrentDateTime - xlInicio, xlTxt);
                end;
            }
            action(ALVPruebaTextBuilderC01)
            {
                ApplicationArea = all;
                Caption = 'Pruebas con TextBuilder';
                Image = EndingText;

                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: TextBuilder;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumIteraciones do begin
                        xlTxt.Append('.');
                    end;
                    Message('Tiempo invertido %1 %2', CurrentDateTime - xlInicio, xlTxt.ToText());
                end;
            }
            action(ALVContarClientesC01)
            {
                ApplicationArea = all;
                Caption = 'Contar Clientes';
                Image = Customer;

                trigger OnAction()
                begin
                    UsoListasF()
                end;
            }
        }
    }
    //Utilizamos la función creada en la Tabla
    trigger OnOpenPage()
    begin
        Rec.GetF();
    end;

    [NonDebuggable]
    local procedure cambiarPasswordF()
    var
        pglALVEntradaDatosVariosC01: Page ALVEntradaDatosVariosC01;
        xlPass: Text;
    begin
        pglALVEntradaDatosVariosC01.CampoF('Contraseña actual', '', true);
        pglALVEntradaDatosVariosC01.CampoF('Nueva contraseña', '', true);
        pglALVEntradaDatosVariosC01.CampoF('Repita nueva contraseña', '', true);
        //Lanzamos la página
        if pglALVEntradaDatosVariosC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            if pglALVEntradaDatosVariosC01.CampoF(xlPass) = Rec.PasswordF() then begin
                if pglALVEntradaDatosVariosC01.CampoF(xlPass) = pglALVEntradaDatosVariosC01.CampoF(xlPass) then begin
                    Rec.PasswordF(xlPass);
                    Message('Contraseña cambiada correctamente');
                end else begin
                    Message('Las contraseñas no coinciden');
                end;
                Message('Tu contraseña actual no coincide');
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    local procedure ExportarVacunaF()
    var
        rlALVCabPlanVacunacionC01: Record ALVCabPlanVacunacionC01;
        TempLALVConfiguracionC01: Record ALVConfiguracionC01 temporary;
        rlALVLinPlanVacunacionC01: Record ALVLinPlanVacunacionC01;
        pglALVListaCabPlanVacunacionC01: Page ALVListaCabPlanVacunacionC01;
        eTipo: Enum ALVTipoC01;
        xlInStream: Instream;
        clSeparador: Label '·', Locked = true; // Locked para que no pase por los traductores
        xlOutStream: OutStream;
        xlNombreFichero: Text;
    begin
        //Commit(); Solo hay que usarlo en casos extremos que no te deja grabar datos
        Message('Selecciona las vacunas que quieres descargar');

        //ESTE SE USA CUANDO SOLO QUEREMOS SELECCIONAR UN RESGISTRO
        // if Page.RunModal(Page::ALVListaCabPlanVacunacionC01, rlALVCabPlanVacunacionC01) in [Action::LookupOK, Action::OK, Action::Yes] then begin

        //ESTE SE USA CUANDO QUEREMOS SELECCONAR VARIOS REGISTROS
        pglALVListaCabPlanVacunacionC01.LookupMode(true); //Para mostrar los botones
        pglALVListaCabPlanVacunacionC01.SetRecord(rlALVCabPlanVacunacionC01); //Para almacenar el registro

        if pglALVListaCabPlanVacunacionC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            TempLALVConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows);
            pglALVListaCabPlanVacunacionC01.GetSelectionFilterF(rlALVCabPlanVacunacionC01);
            //Obtenemos el dataset
            //Si queremos modificar en el primer parametro del FindSet tenemos que poner true, si solo queremos leer tenemos que poner false
            //Si modificamos la clave principal o algun campo del filtro tenemos que poner en el segundo parametro del FindSet true, sino false

            //ESCRIBIMOS LAS CABECERAS
            if rlALVCabPlanVacunacionC01.FindSet(false) then begin //Cada vez que hace un next no se va a SQL, por lo que mejora el rendimiento del FindFirst y del FindLast
                repeat
                    xlOutStream.WriteText('C');
                    xlOutStream.WriteText(clSeparador);
                    xlOutStream.WriteText(rlALVCabPlanVacunacionC01.CodigoCabecera);
                    xlOutStream.WriteText(clSeparador);
                    xlOutStream.WriteText(rlALVCabPlanVacunacionC01.Descripcion);
                    xlOutStream.WriteText(clSeparador);
                    xlOutStream.WriteText(rlALVCabPlanVacunacionC01.EmpresaVacunadora);
                    xlOutStream.WriteText(clSeparador);
                    xlOutStream.WriteText(Format(rlALVCabPlanVacunacionC01.FechaVacunacionPlanificada));
                    xlOutStream.WriteText(clSeparador);
                    xlOutStream.WriteText(rlALVCabPlanVacunacionC01.NombreEmpresaVacunadoraF());
                    xlOutStream.WriteText();

                    rlALVLinPlanVacunacionC01.SetRange(CodigoLineas, rlALVCabPlanVacunacionC01.CodigoCabecera); //SetRange es más rápido y es más cómodo que SetFilter
                    //rlALVLinPlanVacunacionC01.SetFilter(CodigoLineas, rlALVCabPlanVacunacionC01.CodigoCabecera);
                    //rlALVLinPlanVacunacionC01.Ascending(true);

                    //ESCRIBIMOS LAS LINEAS
                    if rlALVLinPlanVacunacionC01.FindSet(false) then begin
                        repeat
                            xlOutStream.WriteText('L');
                            xlOutStream.WriteText(clSeparador);
                            xlOutStream.WriteText(rlALVLinPlanVacunacionC01.CodigoLineas);
                            xlOutStream.WriteText(clSeparador);
                            xlOutStream.WriteText(Format(rlALVLinPlanVacunacionC01.N_Linea));
                            xlOutStream.WriteText(clSeparador);
                            xlOutStream.WriteText(rlALVLinPlanVacunacionC01.NombreClienteF());
                            xlOutStream.WriteText(clSeparador);
                            xlOutStream.WriteText(Format(rlALVLinPlanVacunacionC01.FechaVacuna));
                            xlOutStream.WriteText(clSeparador);
                            xlOutStream.WriteText(Format(rlALVLinPlanVacunacionC01.FechaSegundaVacuna));
                            xlOutStream.WriteText(clSeparador);
                            xlOutStream.WriteText(rlALVLinPlanVacunacionC01.CodigoVacuna);
                            xlOutStream.WriteText(clSeparador);
                            xlOutStream.WriteText(rlALVLinPlanVacunacionC01.CodigoOtros);
                            xlOutStream.WriteText(clSeparador);
                            xlOutStream.WriteText(rlALVLinPlanVacunacionC01.DescripcionVariosF(eTipo::Vacuna, rlALVLinPlanVacunacionC01.CodigoVacuna));
                            xlOutStream.WriteText(clSeparador);
                            xlOutStream.WriteText(rlALVLinPlanVacunacionC01.DescripcionVariosF(eTipo::Otros, rlALVLinPlanVacunacionC01.CodigoOtros));
                            xlOutStream.WriteText();
                        until rlALVLinPlanVacunacionC01.Next() = 0;
                    end;
                until rlALVCabPlanVacunacionC01.Next() = 0;
                TempLALVConfiguracionC01.MiBlob.CreateInStream(xlInStream);
                xlNombreFichero := 'Vacunas.csv';
                if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
                    Error('Imposible descargar el fichero');
                end;
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    local procedure MostrarClientesGrisF()
    var
        rlCustomer: Record Customer;
        rlCompany: Record Company;
        xlFiltroGrupo: Integer;
    begin
        // Mostrar la lista de empresas de la BBDD y de la seleccionada mostrar sus clientes grises
        if AccionAceptadaF(page.RunModal(page::Companies, rlCompany)) then begin
            rlCustomer.ChangeCompany(rlCompany.Name);
            rlCustomer.FilterGroup(xlFiltroGrupo + 20); // Hacemos que el filtro de grupo no muestre distintos datos al cambiar el filtroc desde BC
            //rlCliente.SetRecFilter(); // Te selecciona el campo como el SetRange pero dentro de la misma tabla
            rlCustomer.SetRange("Location Code", 'GRIS');
            rlCustomer.FilterGroup(xlFiltroGrupo - 20); // Restablecemos el filtro de grupo para que funcione
            if AccionAceptadaF(Page.RunModal(page::"Customer List", rlCustomer)) then begin
                Message('Proceso realizado correctamente');
            end else begin
                Error('Proceso cancelado por el usuario');
            end;
        end;
    end;

    local procedure ImportarVacunaF()
    var
        rlALVCabPlanVacunacionC01: Record ALVCabPlanVacunacionC01;
        rlALVLinPlanVacunacionC01: Record ALVLinPlanVacunacionC01;
        TempLALVConfiguracionC01: Record ALVConfiguracionC01 temporary;
        xlInStream: Instream;
        xlLinea: Text;
        clSeparador: Label '·', Locked = true; // Locked para que no pase por los traductores
        xlFecha: Date;
        xlNumLinea: Integer;
    begin
        TempLALVConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows);
        if UploadIntoStream('', xlInStream) then begin
            while not xlInStream.EOS do begin // Para comprobar si estamos al final del Stream (END OF STREAM)
                xlInStream.ReadText(xlLinea);
                // Insertar cabecera o linea en función del primer caracter de la linea
                case true of
                    xlLinea[1] = 'C':
                        // Alta cabecera
                        begin
                            rlALVCabPlanVacunacionC01.Init();
                            rlALVCabPlanVacunacionC01.Validate(CodigoCabecera, xlLinea.Split(clSeparador).Get(2));
                            rlALVCabPlanVacunacionC01.Insert(true);
                            rlALVCabPlanVacunacionC01.Validate(Descripcion, xlLinea.Split(clSeparador).Get(3));
                            rlALVCabPlanVacunacionC01.Validate(EmpresaVacunadora, xlLinea.Split(clSeparador).Get(4));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(5)) then begin
                                rlALVCabPlanVacunacionC01.Validate(FechaVacunacionPlanificada, xlFecha);
                            end;
                            rlALVCabPlanVacunacionC01.Modify(true);
                        end;
                    xlLinea[1] = 'L':
                        // Alta línea
                        begin
                            rlALVLinPlanVacunacionC01.Init();
                            rlALVLinPlanVacunacionC01.Validate(CodigoLineas, xlLinea.Split(clSeparador).Get(2));
                            if Evaluate(xlNumLinea, xlLinea.Split(clSeparador).Get(3)) then begin
                                rlALVLinPlanVacunacionC01.Validate(N_Linea, xlNumLinea);
                            end;
                            rlALVLinPlanVacunacionC01.Insert(true);
                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(4)) then begin
                                rlALVLinPlanVacunacionC01.Validate(FechaVacuna, xlFecha);
                            end;
                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(5)) then begin
                                rlALVLinPlanVacunacionC01.Validate(FechaSegundaVacuna, xlFecha);
                            end;
                            rlALVLinPlanVacunacionC01.Validate(CodigoVacuna, xlLinea.Split(clSeparador).Get(7));
                            rlALVLinPlanVacunacionC01.Validate(CodigoOtros, xlLinea.Split(clSeparador).Get(8));
                            rlALVLinPlanVacunacionC01.Modify(true);
                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end
            end;
        end else begin
            Error('Imposible subir el fichero');
        end;
    end;

    procedure AccionAceptadaF(pAction: Action) returnValue: Boolean
    begin
        exit(pAction in [Action::LookupOK, pAction::OK, pAction::Yes]); // Si se cumple devuelve true
    end;

    local procedure UsoListasF()
    var
        rlCustLedgerEntry: Record "Cust. Ledger Entry";
        xlPaginaFiltros: FilterPageBuilder;
        xlLista: List of [Code[20]];
        clMovsCliente: Label 'Movs Clientes';
    begin
        xlPaginaFiltros.AddRecord(clMovsCliente, rlCustLedgerEntry);
        xlPaginaFiltros.AddField(clMovsCliente, rlCustLedgerEntry."Sell-to Customer No.");
        xlPaginaFiltros.AddField(clMovsCliente, rlCustLedgerEntry."Posting Date");
        if xlPaginaFiltros.RunModal() then begin
            rlCustLedgerEntry.SetView(xlPaginaFiltros.GetView(clMovsCliente, true));
            rlCustLedgerEntry.SetLoadFields("Sell-to Customer No.");
            if rlCustLedgerEntry.FindSet(false) then begin
                repeat
                    if not xlLista.Contains(rlCustLedgerEntry."Sell-to Customer No.") then begin
                        xlLista.Add(rlCustLedgerEntry."Sell-to Customer No.");
                    end;
                until rlCustLedgerEntry.Next() = 0;
            end;
            page.Run(Page::"Customer Ledger Entries", rlCustLedgerEntry); // Muestra los registros filtrados
            Message('En movimientos de cliente hay %1 clientes, con los filtros seleccionados', xlLista.Count);
        end;
    end;

    var
        xNumIteraciones: Integer;
}