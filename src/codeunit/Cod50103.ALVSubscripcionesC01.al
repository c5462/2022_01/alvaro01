codeunit 50103 "ALVSubscripcionesC01"
{
    SingleInstance = true;

    //Mostrar una confirmación para confirmar si queremos cerrar o no la ventana de clientes
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnBeforePostSalesDocF(var SalesHeader: Record "Sales Header")
    begin
        SalesHeader.TestField("External Document No.");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnAfterPostSalesDocF(var SalesHeader: Record "Sales Header")
    var
        culALVFuncionesAppC01: Codeunit ALVFuncionesAppC01;
    begin
        culALVFuncionesAppC01.CodeunitSalesPostOnAfterPostSalesDocF(SalesHeader);
    end;

    //Mostrar un mensaje al terminar de registrar una factura
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPostPurchaseDoc', '', false, false)]
    local procedure CodeunitPurchPostOnAfterPostPurchaseDocF(PurchInvHdrNo: Code[20])
    begin
        Message('La factura número %1 se ha registrado correctamente por el usuario %2', PurchInvHdrNo, UserId);
    end;

    //Mostrar un mensaje al terminar de registrar una factura 2 (MÉTODO ÓPTIMO)
    /*[EventSubscriber(ObjectType::Table, Database::"ALVLinPlanVacunacionC01", 'OnBeforeCalcularFechaProxVacunaF', '', false, false)]
    local procedure CodeunitPurchPostOnBeforePostPurchaseDocF(xRec: Record ALVLinPlanVacunacionC01);
    var
        culALVFuncionesAppC01: Codeunit "ALVFuncionesAppC01";
    begin
        Sleep(5000);
    end;*/
}