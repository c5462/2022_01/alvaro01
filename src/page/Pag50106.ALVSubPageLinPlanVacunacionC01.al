page 50106 "ALVSubPageLinPlanVacunacionC01"
{
    Caption = 'Subpágina Líneas Plan Vacunación';
    PageType = ListPart;
    SourceTable = ALVLinPlanVacunacionC01;
    UsageCategory = Lists;
    AutoSplitKey = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.CodigoLineas)
                {
                    ToolTip = 'Especifica el valor de la variable Código';
                    ApplicationArea = All;
                    Editable = false;
                    Visible = false;
                }
                field("N_Linea"; Rec.N_Linea)
                {
                    Caption = 'Nº Línea';
                    ToolTip = 'Especifica el valor de la variable Nº Línea';
                    ApplicationArea = All;
                    Editable = false;
                    Visible = false;
                }
                field(Cliente; Rec.Cliente)
                {
                    Caption = 'Código Cliente';
                    ToolTip = 'Especifica el valor de la variable Código Cliente';
                    ApplicationArea = All;
                }
                field(NombreCliente; Rec.NombreClienteF())
                {
                    Caption = 'Nombre Cliente';
                    ToolTip = 'Especifica el valor de la variable Nombre Cliente';
                    ApplicationArea = All;
                    TableRelation = ALVLinPlanVacunacionC01.Cliente;
                }
                field(FechaVacuna; Rec.FechaVacuna)
                {
                    Caption = 'Fecha 1ª Vacunación';
                    ToolTip = 'Especifica el valor de la variable Nombre Cliente';
                    ApplicationArea = All;
                }
                field(FechaSegundaVacuna; Rec.FechaSegundaVacuna)
                {
                    Caption = 'Fecha 2ª Vacunación';
                    ToolTip = 'Especifica el valor de la variable Nombre Cliente';
                    ApplicationArea = All;
                }
                field(TipoVacuna; Rec.CodigoVacuna)
                {
                    ToolTip = 'Specifies the value of the Tipo Vacuna field.';
                    ApplicationArea = All;
                    TableRelation = ALVVariosC01.Codigo where(Tipo = const(Vacuna), Bloqueado = const(false));
                }
                field(TipoOtros; Rec.CodigoOtros)
                {
                    ToolTip = 'Specifies the value of the Tipo Otros field.';
                    ApplicationArea = All;
                    TableRelation = ALVVariosC01.Codigo where(Tipo = const(Otros), Bloqueado = const(false));
                }
                field(DescripcionVacuna; Rec.DescripcionVariosF(rALVTipoC01.Tipo::Vacuna, Rec.CodigoVacuna))
                {
                    Caption = 'Descripción Vacuna';
                    ApplicationArea = All;
                }
                field(DescripcionOtros; Rec.DescripcionVariosF(eALVTipoC01::Otros, Rec.CodigoOtros))
                {
                    Caption = 'Descripción Otros';
                    ApplicationArea = All;
                }
            }
        }
    }

    var
        rALVTipoC01: Record ALVVariosC01;
        eALVTipoC01: Enum ALVTipoC01;

    /*trigger OnNewRecord(BelowRec: Boolean)
    var
        rlCabecera: Record ALVCabPlanVacunacionC01;
    begin
        if rlCabecera.Get(Rec.CodigoLineas) then begin
            Rec.Validate(FechaVacuna, rlCabecera.FechaVacunacionPlanificada);
        end;
    end;*/
}