report 50106 "ALVGenericoVentasC01"
{
    Caption = 'Genérico Ventas';
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './layout/GenericoVentas.rdlc';

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            dataitem("Sales Invoice Line"; "Sales Invoice Line")
            {
                DataItemLink = "Document No." = field("No.");
                trigger OnAfterGetRecord()
                begin
                    ALVLinGenericaVentasC01.Init();
                    ALVLinGenericaVentasC01.TransferFields("Sales Invoice Line");
                    ALVLinGenericaVentasC01.Insert(false);
                end;
            }
            //Hace el trigger antes de cargar los datos del dataset
            trigger OnPreDataItem()
            begin
                if "Sales Invoice Header".GetFilters = '' then begin
                    //Si no lo lee, se salta el bloque entero
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            begin
                ALVCabeceraGenericaVentasC01.Init();
                ALVCabeceraGenericaVentasC01.TransferFields("Sales Invoice Header");
                ALVCabeceraGenericaVentasC01.Insert(false);

                xCaptionInforme := 'Factura';
            end;
        }

        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            dataitem("Sales Shipment Line"; "Sales Shipment Line")
            {
                DataItemLink = "Document No." = field("No.");
                trigger OnAfterGetRecord()
                begin
                    ALVLinGenericaVentasC01.Init();
                    ALVLinGenericaVentasC01.TransferFields("Sales Shipment Line");
                    ALVLinGenericaVentasC01.Insert(false);
                end;
            }
            //Hace el trigger antes de cargar los datos del dataset
            trigger OnPreDataItem()
            begin
                if "Sales Shipment Header".GetFilters = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            begin
                ALVCabeceraGenericaVentasC01.Init();
                ALVCabeceraGenericaVentasC01.TransferFields("Sales Invoice Header");
                ALVCabeceraGenericaVentasC01.Insert(false);

                xCaptionInforme := 'Albarán';
            end;
        }

        dataitem(ALVCabeceraGenericaVentasC01; ALVCabeceraGenericaVentasC01)
        {
            column(No_ALVCabeceraGenericaVentasC01; "No.")
            {
            }
            column(SelltoCustomerNo_ALVCabeceraGenericaVentasC01; "Sell-to Customer No.")
            {
            }
            column(PostingDate_ALVCabeceraGenericaVentasC01; "Posting Date")
            {
            }
            column(xCaptionInforme; xCaptionInforme)
            {

            }
            dataitem(ALVLinGenericaVentasC01; ALVLinGenericaVentasC01)
            {


                DataItemLink = "Document No." = field("No.");
                column(No_ALVLinGenericaVentasC01; "No.")
                {
                }
                column(Description_ALVLinGenericaVentasC01; Description)
                {
                }
                column(Quantity_ALVLinGenericaVentasC01; Quantity)
                {
                }

                trigger OnAfterGetRecord()
                begin

                end;
            }
            trigger OnAfterGetRecord()
            begin

            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    /*
                    field(Name; SourceExpression)
                    {
                        ApplicationArea = All;

                    }*/
                }
            }
        }
    }

    //Otra forma de hacerlo
    /*
    procedure SetTipoInforme(pOtion: Option Factura,Albaran)
    begin
        xOption := pOtion;
    end;
    procedure GetTipoInforme(): Option
    begin
        exit(xOption);
    end;
    var
        xOption: Option Factura,Albaran;*/

    var
        xCaptionInforme: Text;
}