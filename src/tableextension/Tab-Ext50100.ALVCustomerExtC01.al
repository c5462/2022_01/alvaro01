/// <summary>
/// TableExtension "ALVCustomerExtC01" (ID 50100) extends Record Customer.
/// </summary>
tableextension 50100 ALVCustomerExtC01 extends Customer
{
    fields
    {
        field(50100; ALVNoVacunasC01; Integer)
        {
            Caption = 'Número de Vacunas';
            FieldClass = FlowField;
            CalcFormula = count(ALVLinPlanVacunacionC01 where(Cliente = field("No.")));
            Editable = false;
        }
        field(50101; ALVFechaUltVacunaC01; Date)
        {
            Caption = 'Fecha Última Vacuna';
            FieldClass = FlowField;
            CalcFormula = max(ALVLinPlanVacunacionC01.FechaVacuna where(Cliente = field("No.")));
            Editable = false;
        }
        field(50102; ALVTipoVacunaC01; Code[20])
        {
            Caption = 'Tipo de Vacuna';
            DataClassification = CustomerContent;
            TableRelation = ALVVariosC01.Codigo where(Tipo = const(Vacuna));

            //Trigger para deshabilitar una vacuna si está bloqueada
            trigger OnValidate()
            var
                rlALVVariosC01: Record ALVVariosC01;
            begin
                if rlALVVariosC01.Get(rlALVVariosC01.Tipo::Vacuna, Rec.ALVTipoVacunaC01) then begin
                    rlALVVariosC01.TestField(Bloqueado, false);
                end;
            end;
        }
        modify(Name)
        {
            trigger OnAfterValidate()
            var
                culALVVariablesAppC01: Codeunit ALVVariablesGlobalesAppC01;
            begin
                culALVVariablesAppC01.NombreF(Rec.Name);
            end;
        }
    }

    trigger OnAfterInsert()
    begin
        SincronizarRegistrosF(xAction::Insert);
    end;

    trigger OnAfterModify()
    begin
        SincronizarRegistrosF(xAction::Modify);
    end;

    trigger OnAfterDelete()
    begin
        SincronizarRegistrosF(xAction::Delete);
    end;

    local procedure SincronizarRegistrosF(pAction: Option)
    var
        rlCompany: Record Company;
        rlCustomer: Record Customer;
    begin
        rlCompany.SetFilter(Name, '<>%1', CompanyName);

        // Recorremos las empresas
        if rlCompany.FindSet(false) then begin
            repeat
                // Cambiar variable local de registro de cliente
                rlCustomer.ChangeCompany(rlCompany.Name);

                // Asignar valores de cliente a la variable local
                rlCustomer.Init();
                rlCustomer.Copy(Rec);

                // Sincronizamos los clientes
                case pAction of
                    // Insertamos los clientes
                    xAction::Insert:
                        rlCustomer.Insert(false);
                    // Modificamos los clientes
                    xAction::Modify:
                        rlCustomer.Modify(false);
                    // Eliminamos los clientes
                    xAction::Delete:
                        rlCustomer.Delete(false);
                end; // Sin TRUE para que no intente crear el

            until rlCompany.Next() = 0;
        end;
    end;

    var
        xAction: Option Insert,Modify,Delete;
}