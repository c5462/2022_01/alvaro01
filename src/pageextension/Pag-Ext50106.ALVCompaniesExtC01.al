pageextension 50106 "ALVCompaniesExtC01" extends Companies
{
    layout
    {
        addlast(Control1)
        {
            field(ALVClientesC01; ClientesF(false))
            {
                ApplicationArea = All;
                Caption = 'Nº Clientes';
                // Para que me haga un DrillDown a los registros de Customer
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    ClientesF(true);
                end;
            }
            field(ALVProveedoresC01; ProveedoresF(false))
            {
                ApplicationArea = All;
                Caption = 'Nº Proveedores';
                // Para que me haga un DrillDown a los registros de Vendor
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    ProveedoresF(true);
                end;
            }
            field(ALVCuentasC01; CuentasF(false))
            {
                ApplicationArea = All;
                Caption = 'Nº Cuentas';
                // Para que me haga un DrillDown a los registros de G/L Account
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    CuentasF(true);
                end;
            }
            field(ALVProductosC01; ProductosF(false))
            {
                ApplicationArea = All;
                Caption = 'Nº Productos';
                // Para que me haga un DrillDown a los registros de Item
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    ProductosF(true);
                end;
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            action(ALVExportarCopiaSeguridadC01SC01)
            {
                Caption = 'Crear copia de seguridad';
                Image = ExportDatabase;
                ApplicationArea = All;

                trigger OnAction()
                begin
                    exportSeguridadEmpresaF();
                end;
            }
            action(ALVImportarCopiaSeguridadC01SC01)
            {
                Caption = 'Importar copia de seguridad';
                Image = ImportDatabase;
                ApplicationArea = All;

                trigger OnAction()
                begin
                    importSeguridadEmpresaF();
                end;
            }
        }
    }

    local procedure ClientesF(pDrillDown: Boolean): Integer
    var
        rlCustomer: Record Customer;
    begin
        exit(DatosTablaF(rlCustomer, pDrillDown));
    end;

    local procedure ProveedoresF(pDrillDown: Boolean): Integer
    var
        rlVendor: Record Vendor;
    begin
        exit(DatosTablaF(rlVendor, pDrillDown));
    end;

    local procedure CuentasF(pDrillDown: Boolean): Integer
    var
        rlGLAccount: Record "G/L Account";
    begin
        exit(DatosTablaF(rlGLAccount, pDrillDown));
    end;

    local procedure ProductosF(pDrillDown: Boolean): Integer
    var
        rlItem: Record Item;
    begin
        exit(DatosTablaF(rlItem, pDrillDown));
    end;

    local procedure DatosTablaF(pRecVariant: Variant; pDrillDown: Boolean): Integer
    var
        rlCompany: Record Company;
        xlRecRef: RecordRef;
    begin
        if pRecVariant.IsRecord then begin
            xlRecRef.GetTable(pRecVariant); // Hago referencia a la tabla, pero no al valor
            if rlCompany.Get(Rec.Name) then begin
                if Rec.Name <> CompanyName then begin
                    xlRecRef.ChangeCompany(Rec.Name);
                end;
                if pDrillDown then begin
                    pRecVariant := xlRecRef;
                    Page.Run(0, pRecVariant);
                end else begin
                    exit(xlRecRef.Count);
                end
            end;
        end;
    end;

    local procedure EjemploRecordRefF()
    var
        xlRecRef: RecordRef;
        xlFieldRef: FieldRef;
        i: Integer;
    begin
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    Message('%1', xlRecRef.FieldIndex(i).Value);
                end;

                // 1ª forma: "Doteando"
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3).Value).ToUpper());

                // 2ª forma: utilizando FieldRef
                xlFieldRef := xlRecRef.Field(3);
                xlFieldRef.Validate(Format(xlFieldRef.Value).ToUpper());

                xlRecRef.Modify(true);

            until xlRecRef.Next() = 0;
            xlRecRef.Open(Database::Item);
        end;
        //xlRecRef.KeyIndex(1).FieldCount //Cuenta los registros de la clave principal
    end;

    // MÉTODO PARA COPIAR EMPRESAS
    local procedure exportSeguridadEmpresaF()
    var
        rlAllObjWithCaption: Record AllObjWithCaption;
        TempLALVConfiguracionC01: Record ALVConfiguracionC01 temporary;
        rlCompany: Record Company;
        culTypeHelper: Codeunit "Type Helper";
        xlRecRef: RecordRef;
        i: Integer;
        xlOutStream: OutStream;
        xlTextBuilder: TextBuilder;
        xlInStream: Instream;
        xlNombreFichero: Text;
    begin
        CurrPage.SetSelectionFilter(rlCompany); // Para poder filtrar por las empresas que queramos
        // 1.Bucle de empresas
        if rlCompany.FindSet(false) then begin
            repeat
                // Escribir en el fichero
                // Inicio empresa
                xlTextBuilder.Append('IE<' + rlCompany.Name + '>');
                xlTextBuilder.Append(culTypeHelper.CRLFSeparator());

                // 2. Bucle de tablas
                rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
                rlAllObjWithCaption.SetFilter("Object ID", '%1|%2|%3|%4', Database::"G/L Account", Database::Customer, Database::Vendor, Database::Item);
                if rlAllObjWithCaption.FindSet(false) then begin
                    repeat
                        // Inicio tablas
                        xlTextBuilder.Append('IT<' + rlAllObjWithCaption."Object Name" + '>');
                        xlTextBuilder.Append(culTypeHelper.CRLFSeparator());

                        // 3. Bucle de registros
                        xlRecRef.Open(rlAllObjWithCaption."Object ID", false, rlCompany.Name);
                        if xlRecRef.FindSet(false) then begin
                            repeat
                                // Inicio registros
                                xlTextBuilder.Append('IR<' + Format(xlRecRef.RecordId) + '>');
                                xlTextBuilder.Append(culTypeHelper.CRLFSeparator());

                                // 4. Bucle de campos
                                for i := 1 to xlRecRef.FieldCount do begin
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin
                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField();
                                        end;
                                        if Format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuilder.Append('<');
                                            xlTextBuilder.Append(xlRecRef.FieldIndex(i).Name);
                                            xlTextBuilder.Append('>');
                                            xlTextBuilder.Append('·');
                                            xlTextBuilder.Append('<');
                                            xlTextBuilder.Append(xlRecRef.FieldIndex(i).Value);
                                            xlTextBuilder.Append('>');
                                            xlTextBuilder.Append(culTypeHelper.CRLFSeparator());
                                        end;
                                    end;
                                end;

                                // Fin registros
                                xlTextBuilder.Append('FR<' + Format(xlRecRef.RecordId) + '>');
                                xlTextBuilder.Append(culTypeHelper.CRLFSeparator());
                            until xlRecRef.Next() = 0;
                        end;
                        // Cerramos registro
                        xlRecRef.Close();

                        // Fin tablas
                        xlTextBuilder.Append('FT<' + rlAllObjWithCaption."Object Name" + '>');
                        xlTextBuilder.Append(culTypeHelper.CRLFSeparator());
                    until xlRecRef.Next() = 0;
                end;

                // Fin empresa
                xlTextBuilder.Append('FE<' + rlCompany.Name + '>');
                xlTextBuilder.Append(culTypeHelper.CRLFSeparator());
            until rlAllObjWithCaption.Next() = 0;
        end;


        // Descargamos el fichero
        TempLALVConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows);
        TempLALVConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows);
        xlOutStream.WriteText(xlTextBuilder.ToText());
        xlNombreFichero := 'Informacion_De_Empresas.csv';
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('Imposible descargar el fichero');
        end;
    end;


    local procedure ValorCampoF(pFieldRef: FieldRef; pValor: Text): Text
    begin
        if pFieldRef.Type = pFieldRef.Type::Option then begin

        end else begin
            exit(pValor);
        end;
    end;

    // MÉTODO PARA IMPORTAR EMPRESAS
    local procedure importSeguridadEmpresaF()
    var
        TempLALVConfiguracionC01: Record ALVConfiguracionC01 temporary;
        xlRecRef: RecordRef;
        xlInStream: Instream;
        xlLinea: Text;
        xlTexto: Text;
        xlNoCampo: Integer;
        xlTexto2: Text;
    begin
        // 1. Subir ficher con la copia de seguridad
        TempLALVConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows);
        if UploadIntoStream('', xlInStream) then begin

            // 2. Recorrer el fichero
            while not xlInStream.EOS do begin
                xlInStream.ReadText(xlLinea);
                case true of
                    // 3. Si es inicio de tabla abrir el registro
                    //xlLinea.Contains('IT'): Si contiene IT<
                    copystr(xlLinea, 1, 3) = 'IT<': //Si empieza por IT<
                        begin
                            xlTexto := CopyStr(xlLinea, 4).TrimEnd('>'); // Nos copia la linea desde el cuarto caracter, y borra el ultimo
                            xlRecRef.Open(devolverNoTablaF(xlTexto)); // Abrimos el registro con el nombre de la tabla
                            xlRecRef.DeleteAll(false);
                        end;

                    // 4. Si es inicio de registro, inicializamos el registro
                    copystr(xlLinea, 1, 3) = 'IR<':
                        begin
                            xlRecRef.Init(); // Inicializamos el registro
                        end;

                    // 5. Por cada campo asignar un valor
                    //xlLinea.Contains('·'): Si contiene el separador
                    //xlLinea[1] = '<': Si empieza por '<'
                    xlLinea.StartsWith('<'): // Si empieza por '<'
                        begin
                            xlTexto := xlLinea.Split('·').Get(1).TrimStart('<').TrimEnd('>');
                            xlNoCampo := numCampoF(xlRecRef.Number, xlTexto);
                            xlTexto2 := xlLinea.Split('·').Get(2).TrimStart('<').TrimEnd('>');
                            if xlRecRef.Field(xlNoCampo).Class = FieldClass::Normal then begin
                                xlRecRef.Field(xlNoCampo).Value := CampoYValorF(xlRecRef.Field(xlNoCampo), xlTexto2);
                            end;
                        end;

                    // 6. Si es fin de registro, insertar el registro
                    copystr(xlLinea, 1, 3) = 'FR<':
                        begin
                            xlRecRef.Insert(false); // Lo ponemos false para que solo importe los datos que hemos exportado
                        end;

                    // 7. Si es fin de tabla cerrar el registro
                    copystr(xlLinea, 1, 3) = 'FT<':
                        begin
                            xlRecRef.Close();
                        end;
                end;
            end;
            Message('Se ha importado correctamente la empresa');
        end else begin
            Error('No se ha podido subir el fichero al sevidor');
        end;
    end;

    local procedure devolverNoTablaF(pNombreTabla: Text): Integer
    var
        rlField: Record Field;
    begin
        rlField.SetRange(TableName, pNombreTabla);
        rlField.SetLoadFields(TableNo);
        if rlField.FindFirst() then begin
            exit(rlField.TableNo);
        end;
    end;

    local procedure numCampoF(pNumTabla: Integer; pNombreCampo: Text): Integer
    var
        rlField: Record Field;
    begin
        rlField.SetRange(TableNo, pNumTabla);
        rlField.SetRange(FieldName, pNombreCampo);
        rlField.SetLoadFields("No.");

        if rlField.FindFirst() then begin
            exit(rlField."No.");
        end;
    end;

    local procedure CampoYValorF(pFieldRef: FieldRef; pValor: Text): Variant
    var
        TempLALVConfiguracionC01: Record ALVConfiguracionC01 temporary;
        TempLCustomer: Record Customer temporary;
        TempLItem: Record Item temporary;
        xlBigInteger: BigInteger;
        xlBool: Boolean;
        xlDateTime: DateTime;
        xlDecimal: Decimal;
        xlGuid: Guid;
        i: Integer;
        xlDate: Date;
        xlTime: Time;
    begin
        case pFieldRef.Type of
            pFieldRef.Type::Integer, pFieldRef.Type::BigInteger:
                begin
                    if Evaluate(xlBigInteger, pValor) then begin
                        exit(xlBigInteger);
                    end;
                end;
            pFieldRef.Type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin
                        if pFieldRef.GetEnumValueCaption(i).ToLower() = pValor.ToLower() then begin
                            exit(i);
                        end
                    end;
                end;
            pFieldRef.Type::Decimal:
                begin
                    if Evaluate(xlDecimal, pValor) then begin
                        exit(xlDecimal);
                    end;
                end;
            pFieldRef.Type::DateTime:
                begin
                    if Evaluate(xlDateTime, pValor) then begin
                        exit(xlDateTime);
                    end;
                end;
            pFieldRef.Type::Boolean:
                begin
                    if Evaluate(xlBool, pValor) then begin
                        exit(xlBool);
                    end;
                end;
            pFieldRef.Type::Blob:
                begin
                    exit(TempLALVConfiguracionC01.MiBlob);
                end;
            pFieldRef.Type::Guid:
                begin
                    if Evaluate(xlGuid, pValor) then begin
                        exit(xlGuid);
                    end;
                end;
            pFieldRef.Type::Media:
                begin
                    exit(TempLCustomer.Image);
                end;
            pFieldRef.Type::MediaSet:
                begin
                    exit(TempLItem.Picture);
                end;
            pFieldRef.Type::Date:
                begin
                    if Evaluate(xlDate, pValor) then begin
                        exit(xlDate);
                    end;
                end;
            pFieldRef.Type::Time:
                begin
                    if Evaluate(xlTime, pValor) then begin
                        exit(xlTime);
                    end;
                end;
            else
                exit(pValor);
        end;
    end;


    /*
    // LO MISMO QUE ARRIBA DE DIFERENTE MANERA
    local procedure ClientesF(pDrillDown: Boolean): Integer
    var
        rlCustomer: Record Customer;
    begin
        // Si el nombre de empresa es distinto a la empresa donde estoy, que me lo cambie a ese
        if Rec.Name <> CompanyName then begin
            rlCustomer.ChangeCompany(Rec.Name);
        end;

        if pDrillDown then begin
            Page.Run(0, rlCustomer); // Si es 'true' abrimos Clientes con el DrillDown
        end else begin
            exit(rlCustomer.Count); // Si es 'false' que me cuente el nº de registros
        end;
    end;

    local procedure ProveedoresF(pDrillDown: Boolean): Integer
    var
        rlVendor: Record Vendor;
    begin
        if Rec.Name <> CompanyName then begin
            rlVendor.ChangeCompany(Rec.Name);
        end;
        if pDrillDown then begin
            Page.Run(0, rlVendor);
        end else begin
            exit(rlVendor.Count);
        end;
    end;

    local procedure CuentasF(pDrillDown: Boolean): Integer
    var
        rlGLAccount: Record "G/L Account";
    begin
        if Rec.Name <> CompanyName then begin
            rlGLAccount.ChangeCompany(Rec.Name);
        end;
        if pDrillDown then begin
            Page.Run(0, rlGLAccount);
        end else begin
            exit(rlGLAccount.Count);
        end;
    end;

    local procedure ProductosF(pDrillDown: Boolean): Integer
    var
        rlItem: Record Item;
        xlRecRef: RecordRef;
    begin
        xlRecRef.GetTable(rlItem); // Hago referencia a la tabla, pero no al valor

        if Rec.Name <> CompanyName then begin
            rlItem.ChangeCompany(Rec.Name);
        end;
        if pDrillDown then begin
            Page.Run(0, rlItem);
        end else begin
            exit(rlItem.Count);
        end;
    end;

    local procedure EjemploRecordRefF()
    var
        xlRecRef: RecordRef;
        xlFieldRef: FieldRef;
        i: Integer;
    begin
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    Message('%1', xlRecRef.FieldIndex(i).Value);
                end;

                // 1ª forma: "Doteando"
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3).Value).ToUpper());

                // 2ª forma: utilizando FieldRef
                xlFieldRef := xlRecRef.Field(3);
                xlFieldRef.Validate(Format(xlFieldRef.Value).ToUpper());

                xlRecRef.Modify(true);

            until xlRecRef.Next() = 0;
            xlRecRef.Open(Database::Item);
        end;
        //xlRecRef.KeyIndex(1).FieldCount //Cuenta los registros de la clave principal
    end;
    */
}