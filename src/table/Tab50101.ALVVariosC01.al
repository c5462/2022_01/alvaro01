table 50101 "ALVVariosC01"
{
    Caption = 'VariosC01';
    DataClassification = ToBeClassified;
    LookupPageId = ALVListaVariosC01;
    DrillDownPageId = ALVListaVariosC01;

    fields
    {
        field(1; Tipo; Enum ALVTipoC01)
        {
            Caption = 'Tipo';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; Codigo; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = OrganizationIdentifiableInformation;
        }

        field(4; Bloqueado; Boolean)
        {
            Caption = 'Bloqueado';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(5; FechaSegundaVac; DateFormula)
        {
            Caption = 'Fecha Segunda Dosis';
            DataClassification = OrganizationIdentifiableInformation;
        }
    }
    keys
    {
        key(PK; Tipo, Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion) { }
    }

    trigger OnDelete()
    begin
        REc.TestField(Bloqueado, true);
    end;
}
