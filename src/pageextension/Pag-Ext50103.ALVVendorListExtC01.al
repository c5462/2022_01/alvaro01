pageextension 50103 "ALVVendorListExtC01" extends "Vendor List"
{
    layout
    {
    }
    actions
    {
        addlast(processing)
        {
            action(ALVImprimeFacturaC01)
            {
                ApplicationArea = all;
                trigger OnAction()
                begin
                    rSalesInvoice.SetRange("No.", '103040');
                    repGenericoVentas.SetTableView(rSalesInvoice);
                    repGenericoVentas.Run();
                end;
            }
            action(ALVImprimeAlbaranC01)
            {
                ApplicationArea = all;
                trigger OnAction()
                begin
                    rSalesShipment.SetRange("No.", '102056');
                    repGenericoVentas.SetTableView(rSalesShipment);
                    repGenericoVentas.Run();
                end;
            }

        }
    }

    var
        rSalesShipment: Record "Sales Shipment Header";
        rSalesInvoice: Record "Sales Invoice Header";
        repGenericoVentas: Report ALVGenericoVentasC01;
}
