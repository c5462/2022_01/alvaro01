pageextension 50102 "ALVCustomerListExtC01" extends "Customer List"
{
    actions
    {
        addfirst(General)
        {
            action(ALVSwitchVariableC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba variables globales a BC';

                trigger OnAction()
                begin
                    Message('El valor actual es %1. Lo cambiamos a %2', cuALVFuncionesAppC01.SwitchF(), not cuALVFuncionesAppC01.SwitchF());
                    cuALVFuncionesAppC01.SwitchF(not cuALVFuncionesAppC01.SwitchF());
                end;
            }

            action(ALVPruebaCapturaErroresC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba codeunit de captura de errores';
                trigger OnAction()
                var
                    culALVCapturarErroresC01: Codeunit ALVCapturarErroresC01;
                begin
                    if culALVCapturarErroresC01.Run() then begin
                        Message('El botón dice que la función se ha ejecutado correctamente');
                    end else begin
                        Message('El botón dice que la función NO se ha ejecutado correctamente.\Motivo: %1', GetLastErrorText());
                    end;

                end;
            }

            action(ALVPruebaCapturaErrores2C01)
            {
                ApplicationArea = all;
                Caption = 'Prueba codeunit de captura de errores 2';
                trigger OnAction()
                var
                    culALVCapturarErroresC01: Codeunit ALVCapturarErroresC01;
                    i: Integer;
                    xlCodProveedor: Code[20];
                begin
                    xlCodProveedor := '10000';
                    for i := 1 to 5 do begin
                        if culALVCapturarErroresC01.CapturarErrorF(xlCodProveedor) then begin
                            Message('Se ha creado el proveedor %1', xlCodProveedor);
                        end;
                        xlCodProveedor := IncStr(xlCodProveedor);
                    end;
                end;
            }

            action(ALVSubscripcionNombreC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba variables globales a BC';

                trigger OnAction()
                begin
                    Message('El valor actual es %1. Lo cambiamos a %2', cuALVFuncionesAppC01.SwitchF(), not cuALVFuncionesAppC01.SwitchF());
                    cuALVFuncionesAppC01.SwitchF(not cuALVFuncionesAppC01.SwitchF());
                end;
            }
        }
        addfirst("&Customer")
        {
            action(ALVExportarClienteC01)
            {
                ApplicationArea = all;
                Caption = 'Exportar Cliente';
                Image = Customer;
                Promoted = true; // Para que salga el botón en cada registro fila de la lista
                Scope = Repeater; // Para que salga el botón en cada registro fila de la lista

                trigger OnAction()
                begin
                    ExportarCteF();
                end;
            }
            action(ALVExportClientC01)
            {
                ApplicationArea = all;
                Caption = 'Exportar Clientes';
                Image = CustomerList;
                Promoted = true; // Para que salga el botón en cada registro fila de la lista
                Scope = Repeater; // Para que salga el botón en cada registro fila de la lista

                trigger OnAction()
                begin
                    ExportarCtesF();
                end;
            }
            action(ALVExportarAExcelC01)
            {
                ApplicationArea = all;
                Caption = 'Exportar clientes a Excel';
                Image = ExportToExcel;
                Promoted = true; // Para que salga el botón en cada registro fila de la lista
                Scope = Repeater; // Para que salga el botón en cada registro fila de la lista

                trigger OnAction()
                begin
                    ExportarExcelF();
                end;
            }
            action(ALVImportarAExcelC01)
            {
                ApplicationArea = all;
                Caption = 'Importar clientes desde Excel';
                Image = ImportExcel;
                Promoted = true; // Para que salga el botón en cada registro fila de la lista
                Scope = Repeater; // Para que salga el botón en cada registro fila de la lista

                trigger OnAction()
                begin
                    ImportarExcelF();
                end;
            }
        }
        addfirst(History)
        {
            action(ALVFacturasAbonosC01)
            {
                ApplicationArea = all;
                Caption = 'Factura y abonos';
                Image = SalesInvoice;

                trigger OnAction()
                begin
                    FacturasAbonosF();
                end;
            }
            action(ALVSumatorio01C01)
            {
                ApplicationArea = all;
                Caption = 'Sumatorio 01';
                Image = NewSum;

                trigger OnAction()
                var
                    rlDetailCustLedgeEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetailCustLedgeEntry.SetRange("Customer No.", Rec."No.");
                    if rlDetailCustLedgeEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlDetailCustLedgeEntry.Amount;
                        until rlDetailCustLedgeEntry.Next() = 0;
                    end;
                    Message('Total = %1. Ha tardados %2', xlTotal, CurrentDateTime - xlInicio);
                end;
            }
            action(ALVSumatorio02C01)
            {
                ApplicationArea = all;
                Caption = 'Sumatorio 02';
                Image = NewSum;

                trigger OnAction()
                var
                    rlDetailedCustLedgeEntry: Record "Detailed Cust. Ledg. Entry";
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetailedCustLedgeEntry.SetRange("Customer No.", Rec."No.");
                    rlDetailedCustLedgeEntry.CalcSums(Amount);
                    Message('Total = %1. Ha tardados %2', rlDetailedCustLedgeEntry.Amount, CurrentDateTime - xlInicio);
                end;
            }
            action(ALVSumatorio03C01)
            {
                ApplicationArea = all;
                Caption = 'Sumatorio 03';
                Image = NewSum;

                trigger OnAction()
                var
                    rlCustLedgeEntry: Record "Cust. Ledger Entry";
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlCustLedgeEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgeEntry.SetLoadFields(Amount); // Hace un select solo del campo que queremos usar, no se trae todos
                    rlCustLedgeEntry.SetAutoCalcFields(Amount); // Hace que el codigo sea mas rapido (Siempre delante del FindSet)
                    if rlCustLedgeEntry.FindSet(false) then begin // Hace el findset del primer registro
                        repeat
                            rlCustLedgeEntry.CalcFields(Amount);
                        until rlCustLedgeEntry.Next() = 0;
                    end;
                    Message('Total = %1. Ha tardados %2', rlCustLedgeEntry.Amount, CurrentDateTime - xlInicio);
                end;
            }
            // NO FUNCIONA, Nos da error porque es un campo calculado. Este no nos queda mas remedio que hacerlo como en la acción anterior.
            action(ALVSumatorio04C01)
            {
                ApplicationArea = all;
                Caption = 'Sumatorio 04';
                Image = NewSum;

                trigger OnAction()
                var
                    rlCustLedgeEntry: Record "Cust. Ledger Entry";
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlCustLedgeEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgeEntry.CalcSums(Amount);
                    Message('Total = %1. Ha tardados %2', rlCustLedgeEntry.Amount, CurrentDateTime - xlInicio);
                end;
            }
            action(ALVPaginaFiltros03C01)
            {
                ApplicationArea = all;
                Caption = 'Página de filtros';
                Image = Filter;

                trigger OnAction()
                begin
                    paginaFiltrosF();
                end;
            }
        }
    }
    var
        cuALVFuncionesAppC01: Codeunit ALVVariablesGlobalesAppC01;

    /// <summary>
    /// GetSelectionFilterF
    /// </summary>
    /// <param name="prlCustomer">VAR Record Customer.</param>
    procedure ALVGetSelectionFilterF(var prlCustomer: Record Customer)
    begin
        CurrPage.SetSelectionFilter(prlCustomer);
    end;

    local procedure AsignaCeldaF(pFila: Integer; pColumna: Integer; pContenido: Text; pNegrita: Boolean; pCursiva: Boolean; var TempPExcelBuffer: Record "Excel Buffer" temporary)
    begin
        TempPExcelBuffer.Init();
        TempPExcelBuffer.Validate("Row No.", pFila);
        TempPExcelBuffer.Validate("Column No.", pColumna);
        TempPExcelBuffer.Insert(true);
        TempPExcelBuffer.Validate("Cell Value as Text", pContenido);
        TempPExcelBuffer.Validate(Bold, pNegrita);
        TempPExcelBuffer.Validate(Italic, pCursiva);
        TempPExcelBuffer.Modify(true);
    end;

    local procedure ExportarCteF()
    var
        TempLALVConfguracionC01: Record ALVConfiguracionC01 temporary;
        rlCustomer: Record Customer;
        xlInstream: Instream;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4', Locked = true;
        clSeparador: Label '·', Locked = true;
        xlOutStream: OutStream;
        xlNombreFichero: Text;
    begin
        TempLALVConfguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows);
        rlCustomer := Rec;
        rlCustomer.Find();
        rlCustomer.CalcFields("Balance (LCY)");
        // rlCustomer.CalcSums(campo1, campo2); Calcula la suma de varios campos
        xlOutStream.WriteText(StrSubstNo(clCadenaCabecera, clSeparador, rlCustomer."No.", rlCustomer.Name, rlCustomer."Balance (LCY)"));
        xlOutStream.WriteText();
        TempLALVConfguracionC01.MiBlob.CreateInStream(xlInstream);
        xlNombreFichero := 'Cliente.csv';
        if not DownloadFromStream(xlInstream, '', '', '', xlNombreFichero) then begin
            Error('Imposible descargar el fichero');
        end;
    end;

    local procedure ExportarCtesF()
    var
        TempLALVConfiguracionC01: Record ALVConfiguracionC01 temporary;
        rlCustomer: Record Customer;
        pglALVCustomerC01: Page "Customer List";
        xlInStream: Instream;
        clSeparador: Label '·', Locked = true; // Locked para que no pase por los traductores
        xlOutStream: OutStream;
        xlNombreFichero: Text;

    begin
        //ESTE SE USA CUANDO QUEREMOS SELECCIONAR VARIOS REGISTROS
        pglALVCustomerC01.LookupMode(true); //Para mostrar los botones
        pglALVCustomerC01.SetRecord(rlCustomer); //Para almacenar el registro

        if pglALVCustomerC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            TempLALVConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows);
            pglALVCustomerC01.ALVGetSelectionFilterF(rlCustomer);
            //Obtenemos el dataset
            //Si queremos modificar en el primer parametro del FindSet tenemos que poner true, si solo queremos leer tenemos que poner false
            //Si modificamos la clave principal o algun campo del filtro tenemos que poner en el segundo parametro del FindSet true, sino false

            //ESCRIBIMOS LAS CABECERAS
            if rlCustomer.FindSet(false) then begin //Cada vez que hace un next no se va a SQL, por lo que mejora el rendimiento del FindFirst y del FindLast
                repeat
                    xlOutStream.WriteText(rlCustomer."No.");
                    xlOutStream.WriteText(clSeparador);
                    xlOutStream.WriteText(rlCustomer."Name");
                    xlOutStream.WriteText(clSeparador);
                    xlOutStream.WriteText(Format(rlCustomer."Balance (LCY)"));
                    xlOutStream.WriteText();
                until rlCustomer.Next() = 0;
                TempLALVConfiguracionC01.MiBlob.CreateInStream(xlInStream);
                xlNombreFichero := 'Clientes.csv';
                if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
                    Error('Imposible descargar el fichero');
                end;
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    local procedure ExportarExcelF()
    var
        rlCustomer: Record Customer;
        TempLExcelBuffer: Record "Excel Buffer" temporary;
        c: Integer;
        f: Integer;
    begin
        // Creamos la hoja Clientes
        TempLExcelBuffer.CreateNewBook('Clientes');

        // Rellenamos las celdas del excel
        AsignaCeldaF(1, 1, Rec.FieldCaption("No."), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 2, Rec.FieldCaption(Name), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 3, Rec.FieldCaption("Responsibility Center"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 4, Rec.FieldCaption("Location Code"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 5, Rec.FieldCaption("Phone No."), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 6, Rec.FieldCaption(Contact), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 7, Rec.FieldCaption("Balance (LCY)"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 8, Rec.FieldCaption("Balance Due (LCY)"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 9, Rec.FieldCaption("Sales (LCY)"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 10, Rec.FieldCaption("Payments (LCY)"), true, true, TempLExcelBuffer);

        // Inicializa el filtro
        CurrPage.SetSelectionFilter(rlCustomer);
        rlCustomer.SetAutoCalcFields("Balance (LCY)", "Balance Due (LCY)", "Sales (LCY)", "Payments (LCY)");
        rlCustomer.SetLoadFields("No.", Name, "Responsibility Center", "Location Code", "Phone No.", Contact, "Balance (LCY)", "Balance Due (LCY)", "Sales (LCY)", "Payments (LCY)");
        if rlCustomer.FindSet(false) then begin
            f := 2;
            repeat
                c := 1;
                AsignaCeldaF(f, c, rlCustomer."No.", false, false, TempLExcelBuffer);
                c += 1;
                AsignaCeldaF(f, c, rlCustomer.Name, false, false, TempLExcelBuffer);
                c += 1;
                AsignaCeldaF(f, c, rlCustomer."Responsibility Center", false, false, TempLExcelBuffer);
                c += 1;
                AsignaCeldaF(f, c, rlCustomer."Location Code", false, false, TempLExcelBuffer);
                c += 1;
                AsignaCeldaF(f, c, rlCustomer."Phone No.", false, false, TempLExcelBuffer);
                c += 1;
                AsignaCeldaF(f, c, rlCustomer.Contact, false, false, TempLExcelBuffer);
                c += 1;
                AsignaCeldaF(f, c, Format(rlCustomer."Balance (LCY)"), false, false, TempLExcelBuffer);
                c += 1;
                AsignaCeldaF(f, c, Format(rlCustomer."Balance Due (LCY)"), false, false, TempLExcelBuffer);
                c += 1;
                AsignaCeldaF(f, c, Format(rlCustomer."Sales (LCY)"), false, false, TempLExcelBuffer);
                c += 1;
                AsignaCeldaF(f, c, Format(rlCustomer."Payments (LCY)"), false, false, TempLExcelBuffer);
                f += 1;
            until rlCustomer.Next() = 0;
        end;

        // Lo preparamos para poder escribir en la hoja
        TempLExcelBuffer.WriteSheet('', '', '');

        // Cerramos el libro
        TempLExcelBuffer.CloseBook();
        TempLExcelBuffer.OpenExcel();
    end;

    local procedure FacturasAbonosF()
    var
        TempLSalesInvoiceHeader: Record "Sales Invoice Header" temporary;
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
        rlSalesCrMemoHeader: Record "Sales Cr.Memo Header";
    begin
        // if rlSalesInvoiceHeader.ReadPermission //Comprueba si tiene permisos de lectura (no recomendable usar)
        // if rlSalesInvoiceHeader.WritePermission //Comprueba si tiene permisos de escritura (no recomendable usar)

        // Recorremos las facturas del cliente, y las insertamos en la tabla temporal
        rlSalesInvoiceHeader.SetRange("Bill-to Customer No.", Rec."No.");
        // Message(rlSalesInvoiceHeader.GetFilter("Bill-to Customer No.")); //Me muestra los filtros de un campo determinado
        // rlSalesInvoiceHeader.SetView(rlSalesInvoiceHeader.GetView()); //Me aplica los filtros que tenga definidos
        if rlSalesInvoiceHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesInvoiceHeader);
                TempLSalesInvoiceHeader."No." := newProcedure(TempLSalesInvoiceHeader);
                TempLSalesInvoiceHeader.Insert(false); //FALSE porque es temporal
            until rlSalesInvoiceHeader.Next() = 0;
        end;
        // Recorremos los abonos, y las insertamos en la table temporal
        rlSalesCrMemoHeader.SetRange("Bill-to Customer No.", Rec."No.");
        // Message(rlSalesInvoiceHeader.GetFilters()); //Me muestra los filtros de todos los campos de un record
        if rlSalesCrMemoHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesCrMemoHeader);
                TempLSalesInvoiceHeader."No." := newProcedure2(TempLSalesInvoiceHeader);
                TempLSalesInvoiceHeader.Insert(false); //FALSE porque es temporal
            until rlSalesCrMemoHeader.Next() = 0;
        end;

        // Mostramos la pagina con facturas y abonos
        Page.Run(0, TempLSalesInvoiceHeader);
    end;


    local procedure ImportarExcelF()
    var
        rlCustomer: Record Customer;
        TempLExcelBuffer: Record "Excel Buffer" temporary;
        xlInStream: Instream;
        xlFicheroCliente: Text;
        xlNombreHoja: Text;
    begin
        if UploadIntoStream('Seleccione un fichero de Excel para exportar los Clientes', '', '|*.xlsx;*.xls', xlFicheroCliente, xlInStream) then begin
            xlNombreHoja := TempLExcelBuffer.SelectSheetsNameStream(xlInStream);
            if xlNombreHoja = '' then begin
                Error('Proceso cancelado');
            end;

            // Abrimos el libro
            TempLExcelBuffer.OpenBookStream(xlInStream, xlNombreHoja);
            TempLExcelBuffer.ReadSheet();

            TempLExcelBuffer.SetCurrentKey("Row No.", "Column No.");
            TempLExcelBuffer.SetFilter("Row No.", '>=2');
            TempLExcelBuffer.SetFilter("Column No.", '<=6');
            if TempLExcelBuffer.FindSet(false) then begin
                repeat
                    case TempLExcelBuffer."Column No." of
                        1:
                            begin
                                rlCustomer.Init();
                                rlCustomer.Validate("No.", TempLExcelBuffer."Cell Value as Text");
                                rlCustomer.Insert(true);
                            end;
                        2:
                            begin
                                rlCustomer.Validate(Name, TempLExcelBuffer."Cell Value as Text");
                                rlCustomer.Modify(true); // No es necesario hacer el modify en todos los case, solo en el último que vaya a entrar
                            end;
                        3:
                            begin
                                rlCustomer.Validate("Responsibility Center", TempLExcelBuffer."Cell Value as Text");
                                rlCustomer.Modify(true); // No es necesario hacer el modify en todos los case, solo en el último que vaya a entrar
                            end;
                        4:
                            begin
                                rlCustomer.Validate("Location Code", TempLExcelBuffer."Cell Value as Text");
                                rlCustomer.Modify(true); // No es necesario hacer el modify en todos los case, solo en el último que vaya a entrar
                            end;
                        5:
                            begin
                                rlCustomer.Validate("Phone No.", TempLExcelBuffer."Cell Value as Text");
                                rlCustomer.Modify(true); // No es necesario hacer el modify en todos los case, solo en el último que vaya a entrar
                            end;
                        6:
                            begin
                                rlCustomer.Validate(Contact, TempLExcelBuffer."Cell Value as Text");
                                rlCustomer.Modify(true); // Necesario para insertar los datos en el resgistro
                            end;
                    end;
                until TempLExcelBuffer.Next() = 0;
            end else begin
                Error('No se ha podido cargar el fichero');
            end;
        end;
    end;

    local procedure paginaFiltrosF()
    var
        rlMovimientos: Record "Cust. Ledger Entry";
        rlCustomer: Record Customer;
#pragma warning disable AL0432
        rlSalesPrice: Record "Sales Price";
#pragma warning restore AL0432
        xlPaginaFiltros: FilterPageBuilder;
        clClientes: Label 'Clientes';
        clMovim: Label 'Movimientos';
        clPrecios: Label 'Precios';
    begin
        xlPaginaFiltros.PageCaption('Seleccione los clientes a procesar'); // Es como un Report (está asociado a una tabla)
        xlPaginaFiltros.AddRecord(clClientes, rlCustomer);
        xlPaginaFiltros.AddField(clClientes, rlCustomer."No.");
        xlPaginaFiltros.AddField(clClientes, rlCustomer.ALVFechaUltVacunaC01);
        xlPaginaFiltros.AddRecord(clMovim, rlMovimientos);
        xlPaginaFiltros.AddRecord(clPrecios, rlSalesPrice);
        xlPaginaFiltros.AddField(clMovim, rlMovimientos."Posting Date");
        xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Item No.");
        xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Starting Date");
        xlPaginaFiltros.AddField(clMovim, rlMovimientos."Document Type");
        if xlPaginaFiltros.RunModal() then begin
            rlCustomer.SetView(xlPaginaFiltros.GetView(clClientes, true));
            // rlVendor.SetView(xlPaginaFiltros.GetView(clClientes, true)); //Se pueden aplicar filtros de una tabla a otra, si consigue encontrar el campo, sino lo ignora y no lo aplica
            if rlCustomer.FindSet(false) then begin
                repeat

                until rlCustomer.Next() = 0;
            end;
            rlMovimientos.SetView(xlPaginaFiltros.GetView(clMovim, true));

            if rlMovimientos.FindSet(false) then begin
                repeat

                until rlMovimientos.Next() = 0;
            end;
            rlSalesPrice.SetView(xlPaginaFiltros.GetView(clPrecios, true));
            if rlSalesPrice.FindSet(false) then begin
                repeat

                until rlSalesPrice.Next() = 0;
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    local procedure newProcedure(var TempLSalesInvoiceHeader: Record "Sales Invoice Header" temporary) returnValue: Code[20]
    begin
        returnValue := CopyStr('A-' + TempLSalesInvoiceHeader."No.", 1, MaxStrLen(returnValue));
    end;

    local procedure newProcedure2(var TempLSalesInvoiceHeader: Record "Sales Invoice Header" temporary) returnValue: Code[20]
    begin
        returnValue := CopyStr('F-' + TempLSalesInvoiceHeader."No.", 1, MaxStrLen(returnValue));
    end;
}
