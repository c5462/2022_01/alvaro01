/// <summary>
/// PageExtension "ALVVendorCardExtC01" (ID 50104) extends Record Customer Card.
/// </summary>

pageextension 50104 ALVVendorCardExtC01 extends "Vendor Card"
{
    layout
    {
        addlast(content)
        {
        }
    }
    actions
    {
        addlast(processing)
        {
            action(ALVCrearClienteC01)
            {
                Caption = 'Crear proveedor como cliente';
                ApplicationArea = All;
                Image = Vendor;

                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                begin
                    if Rec.Get(rlCustomer.Name) then begin
                        Message('El proveedor ya existe');
                    end else begin
                        rlCustomer.Init();
                        rlCustomer.TransferFields(Rec, true, true);
                        rlCustomer.Insert();
                        Message('Proveedor creado correctamente');
                    end;
                end;
            }
        }
    }
}
