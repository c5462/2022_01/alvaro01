codeunit 50104 "ALVFuncionesAppC01"
{
    procedure MensajeFraCompraF(PurchInvHdrNo: Code[20])
    begin
        if PurchInvHdrNo <> '' then begin
            Message('El usuario %1 ha creado la factura de compra %2', UserId, PurchInvHdrNo);
        end;
    end;

    // Un PROCEDIMIENTO no devuelve nada, mientras que una FUNCIÓN si
    procedure EjemploVariablesEntradaSalidaF(var pTexto: Text)  //Hace falta ponerle var para que la variable sea almacenada en global
    begin
        pTexto := 'Tecon servicios';
    end;

    procedure EjemploObjetosEntradaSalida02F(pTexto: TextBuilder)   //No hace falta ponerle var, porque un objeto por defecto es de tipo var
    begin
        pTexto.Append('Tecon servicios');
    end;

    procedure EjemploBingoF()
    var
        mlALVNumerosC01: array[99] of Text;
        i: Integer;
        xlNumeroElemArray: Integer;
        x: Integer;
    begin
        Randomize(783224);
        for i := 1 to ArrayLen(mlALVNumerosC01) do begin
            mlALVNumerosC01[i] := Format(i);
        end;
        xlNumeroElemArray := ArrayLen(mlALVNumerosC01);
        repeat
            x := Random(xlNumeroElemArray);
            Message('El %1', mlALVNumerosC01[x]);
            //Message(mlALVNumerosC01[xlRandomObtenido]);
            mlALVNumerosC01[x] := '';
            xlNumeroElemArray := CompressArray(mlALVNumerosC01);
        until xlNumeroElemArray <= 0;
        Message('BINGO TERMINADO. GRACIAS POR PARTICIPAR');
    end;

    procedure UltimoNumLineaF(pRec: Record "Sales Line"): Integer
    var
        rlRec: Record "Sales Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end
    end;

    procedure UltimoNumLineaF(pRec: Record "Purchase Line"): Integer
    var
        rlRec: Record "Purchase Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end
    end;

    procedure UltimoNumLineaF(pRec: Record "Gen. Journal Line"): Integer
    var
        rlRec: Record "Gen. Journal Line";
    begin
        rlRec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");
        rlRec.SetRange("Journal Template Name", pRec."Journal Template Name");
        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end
    end;

    //Esta función sobrecarga otra, para añadirle un parámetro sin modificar 100 lineas
    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text)
    begin
        EjemploSobrecargaF(p1, p2, p3, false);
    end;

    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text; p4: Boolean)
    var
        xlNuevoNumero: Decimal;
    begin
        p1 := 1;
        p2 := 10.5;
        p3 := 'Hola mundo';
        if p4 then begin
            xlNuevoNumero := p1 * p2;
        end;
    end;

    procedure UsoFuncionEjemploF()
    begin
        EjemploSobrecargaF(10, 34.2, 'Te amo');
    end;

    procedure SintaxisF()
    var
        rlCustomer: Record Customer;
        xBool: Boolean;
        xlFecha: Date;
        x: Decimal;
        Ventana: Dialog;
        i: Integer;
        xlLong: Integer;
        xlMaxLong: Integer;
        xlPos: Integer;
        z: Integer;
        clSeparador: Label '·', Locked = true;
        xlCampos: List of [Text];
        xlTexto: Text;
        xlHora: Time;
    begin
        //Asignacion y puntuacion :=
        i := 1;
        i := 1 + 10;
        i += 10;

        //Concatenacion
        xltexto := 'Alvaro';
        xltexto := xltexto + ' Martinez';
        xltexto += ' Jimenez';
        xltexto := StrSubstNo('%1 %2 %3', 'Alvaro', 'Martinez', 'Jimenez');

        //Operadores
        i := Round(x + z, 1); //Sin decimales (precisión 1)
        i := Round(x + z, 2); //Números pares (múltiplos de 2)
        i := Round(x + z, 0.01); //Precisión 0.01
        i := Round(x + z, 0.5, '<'); //Redondea al entero más bajo, utilizando la precisión 0.5
        i := Round(x + z, 0.1, '>'); //Redondea al entero más alto, utilizando la precisión 0.1
        i := 9 div 2; //Divide 9/2 y se queda con la parte entera (i = 4)
        i := 9 div 2; //Divide 9/2 y se queda con el resto (i = 1)
        i := Abs(-9); //Valor absoluto

        //Operadores lógicos 
        xBool := not true;  //Negación
        xBool := false and true; //And
        xBool := false or true;  //Or
        xBool := false xor true; //Xor

        //Operadores relacionales
        xBool := 1 < 2; //Devuelve true
        xBool := 1 > 2; //Devuelve false
        xBool := 1 <= 2; //Devuelve true
        xBool := 1 >= 2; //Devuelve false
        xBool := 1 = 2; //Devuelve false
        xBool := 1 <> 2; //Devuelve true
        xBool := 1 in [1, 'B', 2, 'y']; //Devuelve true si '1' está entre esos valores
        xBool := 'a' in ['a' .. 'z']; //Devuelve true si 'a' está en ese rango de valores

        //FUNCIONES MUY USADAS
        Message('Proceso finalizado');
        Message('El cliente %1 no tiene saldo pendiente de pago.', rlCustomer.Name);
        Message('Estamos en el año %1%2%1%1', 2, 0); //Estamos en el año 2022
        Error('Proceso cancelado por el usuario');
        Error('Proceso cancelado por el usuario %1', UserId);

        //Esto es útil para cambiar la funcionalidad de un botón del estándar, por ejemplo, 
        //nos suscribimos al OnBeforeAction(), hacemos nuestro proceso, lo grabamos, y salimos 
        //con el error en blanco (rollback), sin que llegue a ejecutar lo del estándar
        //No lanza error ni mensaje pero como es un error para el proceso (rollback)
        Commit();
        Error('');


        xBool := Confirm('¿Confirma que desea registrar la factura %1?', false, 'FAC001'); //Si no pongo nada por defecto es true, pero es mejor poner false, para que por defecto si el usuario no lee y le da a Intro se salga con un 'No'

        i := StrMenu('Enviar, Facturar, Enviar y facturar', 3, '¿Como desea registrar');
        case i of
            0:
                Error('Proceso cancelado por el usuario');
            1:
                Message('Pedido enviado'); //Enviar pedido
            2:
                Message('Pedido facturado'); //Facturar pedido
            3:
                Message('Pedido enviado y facturado'); //Enviar y facturar pedido
        end;

        //Siempre que vayamos a mostrar cosas al usuario se usa el GuiAllowed
        if GuiAllowed then begin
            Ventana.Open('Procesando bucle i #1##########\' +
                         'Procesando bucle x #2##########'); //Lo primero que hay que hacer es abrir la ventana, si no da error al usar Update()
        end;
        for i := 1 to 1000 do begin
            if GuiAllowed then begin
                Ventana.Update(1, i); //Que vaya actualizando el control '1' con valor 'i'
            end;
            for x := 1 to 1000 do begin
                if GuiAllowed then begin
                    Ventana.Update(2, x);
                end;
            end;
        end;
        if GuiAllowed then begin
            Ventana.Close();
        end;

        //Funciones de cadenas de texto
        xlMaxLong := MaxStrLen(xlTexto); //Longitud maxima que puede tener una cadena
        Message('%1', xlMaxLong);
        xlTexto := CopyStr('ABC', 2, 1); //Copia una cadena en otra cadena, desde la posicion que le digamos y con el numero de caracteres que queramos
        Message(xlTexto); //B
        xlTexto := CopyStr('ABC', 2);
        Message(xlTexto); //BC
        xlTexto := xlTexto.Substring(2);
        xlPos := StrPos('Elefante', 'e'); //Devuelve la posicion de un string (Elefante, E)
        Message('%1', xlPos); //3
        xlTexto := 'Elefante';
        xlPos := xlTexto.IndexOf('e'); //Devuelve la posicion de un string
        Message('%1', xlPos); //3
        xlLong := StrLen('ABCD      '); //Devuelve la longitud de una cadena
        Message('%1', xlLong); //4 (no cuenta los espacios)
        xlTexto := UpperCase(xlTexto); //Pasar a mayúscula
        xlTexto := xlTexto.ToUpper(); //Pasar a mayúscula
        xlTexto := LowerCase(xlTexto); //Pasar a minúscula
        rlCustomer.Validate("Search Name", rlCustomer.Name.ToUpper());
        xlTexto := xlTexto.TrimStart('\').TrimEnd('\').Trim() + '\'; //Trim borra los espacio al principio y al final
        xlTexto := xlTexto.ToLower(); //Pasar a minúscula
        xlTexto := '123,456,Tecon Servicios, s.l.,789';
        Message(SelectStr(4, xlTexto)); //s.l.
        xlTexto := '123,456,Tecon Servicios, s.l.,789';
        Message(xlTexto.Split(clSeparador).Get(4)); //789
        xlCampos := xlTexto.Split(clSeparador);
        Message(xlCampos.Get(4)); //789
        Message(xlTexto.Split(clSeparador).Get(1)); //123
        Message(xlTexto.Split(clSeparador).Get(2)); //456
        Message(xlTexto.Split(clSeparador).Get(3)); //Tecon Servicios, s.l.
        Message(xlTexto.Split(clSeparador).Get(4)); //789
        xlTexto := '21-05-2022';
        Evaluate(xlFecha, ConvertStr(xlTexto, '-', '/')); //Evalua si se puede convertir el campo (conveniente usarlo con IF) ConvertStr -> Convierte los caracteres de una cadena a otra cadena
        Evaluate(xlFecha, xlTexto.Replace('-', '/').Replace(',', '.')); //Remplaza los valores del primer campo por los del segundo
        xlTexto := 'Elefante';
        Message(DelChr(xlTexto, '<', 'e')); //Elimina la e del principio
        Message(DelChr(xlTexto, '>', 'e')); //Elimina la e del final
        Message(DelChr(xlTexto, '=', 'e')); //Elimina todas las e que haya en medio
        Message(DelChr(xlTexto, '<=>', 'e')); //Elimina las e del principio, del medio y del final
        xlPos := Power(3, 2); //Te hace la potencia de un número 3^2
        xlTexto := UserId(); //Devuelve el usuario logueado
        xlTexto := CompanyName(); //Devuelve el nombre de la empresa
        xlFecha := Today(); //Devuelve la fecha de hoy
        xlFecha := WorkDate(); // Devuelve la fecha de trabajo  
        xlHora := Time(); //Devuelve la hora actual
    end;

    procedure RightF(pCadena: Text; pNumCaracteres: Integer) xlRight: Text
    begin
        xlRight := CopyStr(pCadena, StrLen(pCadena) - pNumCaracteres + 1);
        if StrLen(xlRight) < pNumCaracteres then begin
            Error('El número de caracteres es mayor que el tamaño del texto');
        end;
    end;

    procedure LeftF(pCadena: Text; pNumCaracteres: Integer) xlLeft: Text
    begin
        xlLeft := CopyStr(pCadena, 1, pNumCaracteres);
        if strlen(xlLeft) < pNumCaracteres then begin
            Error('El número de caracteres es mayor que el tamaño del texto');
        end;
    end;

    procedure StreamsF()
    var
        TempLALVConfiguracionC01: Record ALVConfiguracionC01 temporary;
        xlInStream: Instream;
        xlOutStream: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        //Los BLOB y los FILE son contenedores de Streams

        //Los OutStream son usados para exportar datos
        TempLALVConfiguracionC01.MiBlob.CreateOutStream(xlOutStream);
        xlOutStream.WriteText('ALV');

        //Los InStream son usados para importar datos
        TempLALVConfiguracionC01.MiBlob.CreateInStream(xlInStream);
        xlInStream.ReadText(xlLinea);
        xlNombreFichero := 'Pedido.csv';
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('Imposible descargar el fichero');
        end;
    end;

    procedure GetClienteF()
    var
        rlCliente: Record Customer;
    begin
        rlCliente.Get('10000');
    end;

    procedure GetLineaPedidoVentaF()
    var
        rlLineaVenta: Record "Sales Line";
    begin
        rlLineaVenta.Get(rlLineaVenta."Document Type"::Order, '101005', 10000);
    end;

    procedure GetDimensionesF()
    var
        rlDimensiones: Record "Dimension Set Entry";
    begin
        rlDimensiones.Get(7, 'GRUPONEGOCIO');
    end;

    procedure GetInfoEmpresaF()
    var
        rlEmpresaInfo: Record "Company Information";
        rlEmpresaInfo2: Record "Company Information";
    begin
        rlEmpresaInfo.Get('');

        rlEmpresaInfo.SetFilter(Name, '%1|%2', '*A*', '*B*');
        if rlEmpresaInfo.FindSet(true, true) then begin
            repeat
                rlEmpresaInfo2 := rlEmpresaInfo;
                rlEmpresaInfo2.Find();
                rlEmpresaInfo2.Validate(Name, DelChr(rlEmpresaInfo.Name, '<=>', 'AB'));
                rlEmpresaInfo2.Modify(true);
            until rlEmpresaInfo.Next() = 0;
        end;
        rlEmpresaInfo.Find();
    end;

    procedure OtrasFuncionesF()
    var
        rlSalesHeader: Record "Sales Header";
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        rlSalesInvoiceHeader.TransferFields(rlSalesHeader);
    end;

    procedure AbrirListaClientesF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodigoCliente';
    begin
        if pNotificacion.HasData(clNombreDato) then begin
            rlCustomer.SetRange("No.", pNotificacion.GetData(clNombreDato));
        end;
        Page.Run(Page::"Customer List", rlCustomer);
    end;

    procedure AbrirFichaClientesF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodigoCliente';
    begin
        if pNotificacion.HasData(clNombreDato) then begin
            rlCustomer.Get(pNotificacion.GetData(clNombreDato));
            Page.Run(Page::"Customer Card", rlCustomer);
        end;
    end;

    procedure CodeunitSalesPostOnAfterPostSalesDocF(rlSalesHeader: Record "Sales Header")
    var
        rlCustomer: Record Customer;
        rlLinVentas: Record "Sales Line";
        xlNotificacion: Notification;
        clLabel2: Label 'La línea %1 del documento %2 Nº %3, con importe %4';
        clLabel1: Label 'El cliente %1 no tiene tipo de vacuna asignado';
    begin
        // Verificamos que el cliente tiene vacunas
        if rlCustomer.Get(rlSalesHeader."Sell-to Customer No.") then begin
            if rlCustomer.ALVTipoVacunaC01 = '' then begin
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo(clLabel1, rlCustomer."No."));
                xlNotificacion.AddAction('Abrir ficha de clientes', Codeunit::ALVFuncionesAppC01, 'AbrirFichaClientesF');
                xlNotificacion.SetData('CodigoCliente', rlCustomer."No.");
                xlNotificacion.Send();
            end;
        end;
        // Comprobamos que no tenga mas de 100 lineas
        rlLinVentas.SetRange("Document No.", rlSalesHeader."No.");
        rlLinVentas.SetRange("Document Type", rlSalesHeader."Document Type");
        rlLinVentas.SetFilter(Amount, '<%1', 100); // Filtra que las lineas no superen los 100
        rlLinVentas.SetLoadFields("Line No.", "Document No.", Amount); // No mete Document type porque viene de cabecera
        if rlLinVentas.FindSet(false) then begin
            repeat
                Clear(xlNotificacion); // Limpiamos la varible de notificiacion
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo(clLabel2, rlLinVentas."Line No.", rlSalesHeader."Document Type", rlLinVentas."Document No.", rlLinVentas.Amount));
                xlNotificacion.Send();
            until rlLinVentas.Next() = 0;
        end;
    end;
}