/// <summary>
/// Report "ALVFacturaVentaC01" (ID 50102). Documento de facturas de venta.
/// </summary>
report 50102 ALVFacturaVentaC01
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './layout/FacturaVenta.rdlc';

    dataset
    {
        dataitem(CabFacturaVenta; "Sales Invoice Header") //Cabeceras
        {
            column(No_CabFacturaVenta; "No.")
            {

            }
            column(xCustAddr1; xCustAddr[1])
            {

            }
            column(xCustAddr2; xCustAddr[2])
            {

            }
            column(xShipAddr1; xShipAddr[1])
            {

            }
            column(xShipAddr2; xShipAddr[2])
            {

            }
            dataitem(Copias; Integer)
            {
                column(ColNum; Number)
                {

                }

                dataitem(LinFacturaVenta; "Sales Invoice Line") //Lineas
                {
                    //DataItemLink = "Document No." = field("No."); //Relacionamos tablas
                    column(No_LinFacturaVenta; "Line No.")
                    {

                    }
                    trigger OnPreDataItem()

                    begin
                        LinFacturaVenta.SetRange("Document No.", CabFacturaVenta."No.");
                    end;
                }

                trigger OnPreDataItem()

                begin
                    Copias.SetRange(Number, 1, xCopias);
                end;
            }

            trigger OnAfterGetRecord()
            begin
                cuFormatAddress.SalesInvSellTo(xCustAddr, CabFacturaVenta);
                cuFormatAddress.SalesInvShipTo(xShipAddr, xCustAddr, CabFacturaVenta);
                rCompanyInfo.Get(); //Se puede usar Get vacío porque es una tabla de configuración, las cuales siempre tienen registro único
                cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(iCopias; xCopias)
                    {
                        Caption = 'Copias';
                        ApplicationArea = All;
                    }
                }
            }
        }

        actions
        {
            area(processing)
            {
                // action(ActionName)
                // {
                //     ApplicationArea = All;

                // }
            }
        }
    }

    //Variables globales
    var
        rCompanyInfo: Record "Company Information"; //Info de la empresa
        cuFormatAddress: Codeunit "Format Address";
        xCopias: Integer;
        xCompanyAddr: array[8] of Text;
        xCustAddr: array[8] of Text;
        xShipAddr: array[8] of Text;
}