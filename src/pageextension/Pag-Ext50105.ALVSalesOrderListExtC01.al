pageextension 50105 "ALVSalesOrderListExtC01" extends "Sales Order List"
{
    actions
    {
        addlast(processing)
        {
            action(ALVExportPedidosVentaC01)
            {
                Caption = 'Exportar pedidos de venta';
                ApplicationArea = All;
                Image = Export;

                trigger OnAction()
                begin
                    ExportarPedidosVentaF();
                end;
            }
            action(ALVImportPedidosVentaC01)
            {
                Caption = 'Importar pedidos de venta';
                ApplicationArea = All;
                Image = Import;

                trigger OnAction()
                begin
                    ImportarPedidosVentaF();
                end;
            }
        }
    }

    local procedure ExportarPedidosVentaF()
    var
        TempLALVConfiguracionC01: Record ALVConfiguracionC01 temporary;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        xlInStream: Instream;
        clSeparador: Label '·', Locked = true; // Locked para que no pase por los traductores
        xlOutStream: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        CurrPage.SetSelectionFilter(rlSalesHeader);
        //Setloadfields de cabeceras
        if rlSalesHeader.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
            TempLALVConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows);
            repeat
                xlOutStream.WriteText('C');
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(rlSalesHeader."No.");
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(Format(rlSalesHeader."Document Type"));
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(rlSalesHeader."Bill-to Name");
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(rlSalesHeader."Bill-to Contact");
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(rlSalesHeader."Bill-to Customer No.");
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(rlSalesHeader."Sell-to Customer No.");
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(Format(rlSalesHeader."Status"));
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(Format(rlSalesHeader."Posting Date"));
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(Format(rlSalesHeader."Due Date"));
                xlOutStream.WriteText(clSeparador);
                xlOutStream.WriteText(Format(rlSalesHeader."Shipment Date"));
                xlOutStream.WriteText();
                //RECORREMOS LAS LÍNEAS
                rlSalesLine.SetRange("Document No.", rlSalesHeader."No.");
                rlSalesLine.SetRange("Document Type", rlSalesHeader."Document Type");
                //Setloadfields de lineas
                if rlSalesLine.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
                    //1 p: campo por el que queremos filtrar el parametro. 2p: "DESDE" | VALOR, si no pongo el 3, estoy diciendo que filtrame el campo 1 con este VALOR. Si le ponemos el 3p: "HASTA".
                    //SETFILTER. 3 valores. Primero: Campo. Segundo: Cadena. Tecero: Valores.
                    repeat
                        xlOutStream.WriteText('L');
                        xlOutStream.WriteText(clSeparador);
                        xlOutStream.WriteText(Format(rlSalesLine."Line No."));
                        xlOutStream.WriteText(clSeparador);
                        xlOutStream.WriteText(Format(rlSalesLine."Document No."));
                        xlOutStream.WriteText(clSeparador);
                        xlOutStream.WriteText(Format(rlSalesLine."Document Type"));
                        xlOutStream.WriteText(clSeparador);
                        xlOutStream.WriteText(Format(rlSalesLine."Type"));
                        xlOutStream.WriteText(clSeparador);
                        xlOutStream.WriteText(rlSalesLine."No.");
                        xlOutStream.WriteText(clSeparador);
                        xlOutStream.WriteText(Format(rlSalesLine.Quantity));
                        xlOutStream.WriteText(clSeparador);
                        xlOutStream.WriteText(Format(rlSalesLine."Unit Price"));
                        xlOutStream.WriteText(clSeparador);
                        xlOutStream.WriteText(rlSalesLine.Description);
                        xlOutStream.WriteText(clSeparador);
                        xlOutStream.WriteText(Format(rlSalesLine.Amount));
                        xlOutStream.WriteText()
                    until rlSalesLine.Next() = 0;
                end;
            until rlSalesHeader.Next() = 0;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        xlNombreFichero := 'PedidosVentas.csv';
        TempLALVConfiguracionC01.MiBlob.CreateInStream(xlInStream);
        xlInStream.ReadText(xlLinea);
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('Error al descargar el documento');
        end;
    end;

    local procedure ImportarPedidosVentaF()
    var
        TempLALVConfiguracionC01: Record ALVConfiguracionC01 temporary;
        rlFichaLog: Record ALVLogC01;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        cdPruebaErrores: Codeunit ALVCapturarErroresC01;
        xlTipoDoc: Enum "Sales Document Type";
        xlInStream: Instream;
        xlNumLinea: Integer;
        clSeparador: Label '·', Locked = true; // Locked para que no pase por los traductores
        xlLinea: Text;
        xlTextoError: Text;
    begin
        TempLALVConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows);
        if UploadIntoStream('', xlInStream) then begin
            while not xlInStream.EOS do begin //EOS END OF STRING. LEE HASTA EL FINAL.
                xlInStream.ReadText(xlLinea);
                //Insertar Cabecera o líneas en funcion del primer caracter de la línea
                case true of
                    xlLinea[1] = 'C':
                        //DAR DE ALTA CABECERA
                        begin
                            rlSalesHeader.Init();
                            if Evaluate(xlTipoDoc, xlLinea.Split(clSeparador).Get(3)) then begin
                                rlSalesHeader.Validate("Document Type", xlTipoDoc);
                            end;
                            rlSalesHeader.Validate("No.", xlLinea.Split(clSeparador).Get(2));
                            rlSalesHeader.Insert(true);
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesHeader, xlLinea, 4, clSeparador) then begin
                                xlTextoError := 'El campo Bill-To-Name no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesHeader, xlLinea, 5, clSeparador) then begin
                                xlTextoError := 'El campo Bill-to Contact no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesHeader, xlLinea, 6, clSeparador) then begin
                                xlTextoError := 'El campo Bill-to Customer No. no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesHeader, xlLinea, 7, clSeparador) then begin
                                xlTextoError := 'El campo Sell-to Customer No. no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesHeader, xlLinea, 8, clSeparador) then begin
                                xlTextoError := 'El campo Status no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesHeader, xlLinea, 9, clSeparador) then begin
                                xlTextoError := 'El campo Posting Date no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesHeader, xlLinea, 10, clSeparador) then begin
                                xlTextoError := 'El campo Due Date no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesHeader, xlLinea, 11, clSeparador) then begin
                                xlTextoError := 'El campo Shipment Date no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            rlSalesHeader.Modify(true);
                        end;
                    xlLinea[1] = 'L':
                        begin
                            rlSalesLine.Init();
                            if Evaluate(xlTipoDoc, xlLinea.Split(clSeparador).Get(4)) then begin
                                rlSalesLine.Validate("Document Type", xlTipoDoc);
                            end;
                            rlSalesLine.Validate("Document No.", xlLinea.Split(clSeparador).Get(3));
                            if Evaluate(xlNumLinea, xlLinea.Split(clSeparador).Get(2)) then begin
                                rlSalesLine.Validate("Line No.", xlNumLinea);
                            end;
                            rlSalesLine.Insert(true);
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesLine, xlLinea, 5, clSeparador) then begin
                                xlTextoError := 'El campo Type no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesLine, xlLinea, 6, clSeparador) then begin
                                xlTextoError := 'El campo No. no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesLine, xlLinea, 7, clSeparador) then begin
                                xlTextoError := 'El campo Quantity no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesLine, xlLinea, 8, clSeparador) then begin
                                xlTextoError := 'El campo Unit Price no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesLine, xlLinea, 9, clSeparador) then begin
                                xlTextoError := 'El campo Description no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            if not cdPruebaErrores.AsignarDatosPedidosVentaF(rlSalesLine, xlLinea, 10, clSeparador) then begin
                                xlTextoError := 'El campo Amount no puede ser modificado. ';
                                rlFichaLog.Validate(Mensaje, StrSubstNo(xlTextoError, GetLastErrorText()));
                            end;
                            rlSalesLine.Modify(true);
                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor. Error (%1', GetLastErrorText());
        end;
    end;


    /// <summary>
    /// GetSelectionFilterF.
    /// </summary>
    /// <param name="prSalesHeader">VAR Record "Sales Header".</param>
    procedure ALVGetSelectionFilterF(var prSalesHeader: Record "Sales Header")
    begin
        CurrPage.SetSelectionFilter(prSalesHeader);
    end;
}
