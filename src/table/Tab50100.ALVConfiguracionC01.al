table 50100 "ALVConfiguracionC01"
{
    Caption = 'Configuración de la app';
    DataClassification = OrganizationIdentifiableInformation;

    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id. Registro';
            DataClassification = SystemMetadata;
        }
        field(2; CodClienteWeb; Code[20])
        {
            Caption = 'Código Cliente para Web';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
            ValidateTableRelation = true;
        }
        field(3; TextoReg; Text[250])
        {
            Caption = 'Texto Registro';
            DataClassification = SystemMetadata;
        }
        field(4; TipoDoc; Enum ALVTipoDoc01C01)
        {
            Caption = 'Tipo de Documento';
            DataClassification = SystemMetadata;
        }
        field(5; NombreCliente; Text[100])
        {
            Caption = 'Nombre del Cliente';
            FieldClass = FlowField;
            CalcFormula = lookup(Customer.Name where("No." = field(CodClienteWeb)));
            Editable = false;
        }
        field(6; TipoTabla; Enum ALVTipoTablaC01)
        {
            Caption = 'Tabla Condicional';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            begin
                //IF BC180
                if xRec.TipoTabla <> Rec.TipoTabla then begin
                    //Rec.CodTabla := ''; //YA NO SE USA PARA ASIGNAR A CAMPOS DE TABLAS
                    Rec.Validate(CodTabla, '');
                end;
                //ELSE
                if xRec.TipoTabla <> Rec.TipoTabla then begin
                    //Rec.CodTabla := ''; //NO SE DEBE USAR PARA ASIGNAR A CAMPOS DE TABLAS (VERSIÓN ANTIGUA)
                    Rec.CodTabla := '';
                end;
            end;
        }
        field(7; CodTabla; Code[20])
        {
            Caption = 'CodTabla';
            DataClassification = SystemMetadata;
            TableRelation = if (TipoTabla = const(Cliente)) Customer else
            if (TipoTabla = const(Proveedor)) Vendor else
            if (TipoTabla = const(Recurso)) Resource else
            if (TipoTabla = const(Empleado)) Employee else
            if (TipoTabla = const(Contacto)) Contact;
        }
        field(8; ColorFondo; Enum ALVColoresC01)
        {
            Caption = 'Color de Fondo';
            DataClassification = SystemMetadata;
        }
        field(9; ColorLetra; Enum ALVColoresC01)
        {
            Caption = 'Color de Letra';
            DataClassification = SystemMetadata;
        }

        field(10; Cod2; Code[20])
        {
            Caption = 'Código 2';
            DataClassification = SystemMetadata;
        }

        field(11; Stock; Decimal)
        {
            Caption = 'Stock';
            //Campo calculado
            FieldClass = FlowField;
            CalcFormula = sum("Item Ledger Entry".Quantity where("Item No." = field(FiltroProducto), "Posting Date" = field(FiltroFechaRegistro), "Entry Type" = field(FiltroTipoMovimiento)));
        }
        field(12; FiltroProducto; Code[20])
        {
            TableRelation = Item."No.";
            Caption = 'Filtro Producto';
            FieldClass = FlowFilter;
        }
        field(13; FiltroFechaRegistro; Date)
        {
            Caption = 'Filtro Fecha Registro';
            FieldClass = FlowFilter;
        }
        field(14; FiltroTipoMovimiento; Enum "Item Ledger Entry Type")
        {
            Caption = 'Filtro Tipo Movimiento';
            FieldClass = FlowFilter;
        }
        field(15; IdPassword; Guid)
        {
            DataClassification = SystemMetadata;
        }
        field(16; MiBlob; Blob)
        {
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
    }

    /// <summary>
    /// Función que ecupera el registro de la tabla actual, y si no existe lo crea. 
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetF(): Boolean
    begin
        if not Rec.Get() then begin
            Rec.Init();
            Rec.Insert(true);
        end;
        exit(true);
    end;

    /// <summary>
    /// Función que devuelve el nombre del cliente, pasado en el parámetro pCodCte. Si no existe devolverá un texto vacío.
    /// </summary>
    /// <param name="pCodCte"></param> 
    /// <returns></returns> Devuelve cliente
    procedure NombreClienteF(pCodCte: Code[20]): Text
    var
        rlCustomer: Record Customer; //Variable local de tipo Registro (rlXXX)
    begin
        if rlCustomer.Get(pCodCte) then begin
            exit(rlCustomer.Name);
        end;
    end;

    /// <summary>
    /// Función que obtiene el nombre de la tabla que elijamos en el campo Tipo de Tabla. 
    /// </summary>
    /// <param name="pTipoTabla">Enum ALVTipoTablaC01.</param>
    /// <param name="pCodTabla">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure NombreTablaSelecF(pTipoTabla: Enum ALVTipoTablaC01; pCodTabla: Code[20]): Text
    var
        rlALVConfiguracionC01: Record ALVConfiguracionC01;
        rlContact: Record Contact;
        rlEmployee: Record Employee;
        rlVendor: Record Vendor;
        rlResource: Record Resource;
    begin
        case pTipoTabla of
            (pTipoTabla::Cliente):
                begin
                    exit(rlALVConfiguracionC01.NombreClienteF(pCodTabla));
                end;
            (pTipoTabla::Contacto):
                begin
                    if rlContact.get(pCodTabla) then begin
                        exit(rlContact.Name)
                    end;
                end;
            (pTipoTabla::Empleado):
                begin
                    if rlEmployee.get(pCodTabla) then begin
                        exit(rlEmployee.FullName());
                    end;
                end;
            (pTipoTabla::Proveedor):
                begin
                    if rlVendor.get(pCodTabla) then begin
                        exit(rlVendor.Name);
                    end;
                end;
            (pTipoTabla::Recurso):
                begin
                    if rlResource.get(pCodTabla) then begin
                        exit(rlResource.Name);
                    end;
                end;
        end
    end;

    [NonDebuggable]
    procedure PasswordF(pPassword: Text)
    begin
        Rec.Validate(IdPassword, CreateGuid());
        Rec.Modify(true);
        IsolatedStorage.Set(Rec.IdPassword, pPassword, DataScope::Company);
    end;

    [NonDebuggable]
    procedure PasswordF() Return: Text
    begin
        if not IsolatedStorage.Get(Rec.IdPassword, DataScope::Company, Return) then begin
            Error('No se encuentra la contraseña');
        end;
    end;
}