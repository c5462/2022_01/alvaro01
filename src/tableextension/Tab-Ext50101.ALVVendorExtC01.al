tableextension 50101 "ALVVendorExtC01" extends Vendor
{
    trigger OnAfterInsert()
    var
        culALVVariablesC01: Codeunit ALVVariablesGlobalesAppC01;
    begin
        Rec.Validate(Rec.Name, culALVVariablesC01.NombreF());
    end;
}