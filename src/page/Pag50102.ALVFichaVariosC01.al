page 50102 "ALVFichaVariosC01"
{
    Caption = 'Ficha De Registros Varios';
    PageType = Card;
    SourceTable = ALVVariosC01;
    AdditionalSearchTerms = 'Curso 01';
    AboutTitle = 'Ficha de tabla varios';
    AboutText = 'Se usará para editar registros de una tabla donde tendremos tipos varios de registros';
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                    AboutTitle = 'Tipo de registro';
                    AboutText = 'Indicará si el tipo es de Vacunas u Otros';
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }

                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
                field(FechaSegundaVac; Rec.FechaSegundaVac)
                {
                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
            }
#pragma warning disable AW0008
            repeater(Informacion)
#pragma warning restore AW0008
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
            }
        }
    }
}
