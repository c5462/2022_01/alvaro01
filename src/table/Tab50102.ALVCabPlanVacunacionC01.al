table 50102 "ALVCabPlanVacunacionC01"
{
    Caption = 'Cabecera Plan De Vacunación';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; CodigoCabecera; Code[20])
        {
            Caption = 'Código';
            DataClassification = SystemMetadata;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = SystemMetadata;
        }
        field(3; FechaVacunacionPlanificada; Date)
        {
            Caption = 'Fecha Inicio Vacunacion Planificada';
            DataClassification = SystemMetadata;
        }
        field(4; EmpresaVacunadora; Code[20])
        {
            Caption = 'Empresa Vacunadora';
            DataClassification = SystemMetadata;
            TableRelation = Vendor."No.";
        }
    }
    keys
    {
        key(PK; CodigoCabecera)
        {
            Clustered = true;
        }
    }
    procedure NombreEmpresaVacunadoraF(): Text
    var
        rlVendor: Record Vendor; //Variable local de tipo Registro (rlXXX)
    begin
        if rlVendor.Get(Rec.EmpresaVacunadora) then begin
            exit(rlVendor.Name);
        end;
    end;

    trigger OnDelete()
    var
        rlLineasPlanVacunacion: Record ALVLinPlanVacunacionC01;
    begin
        rlLineasPlanVacunacion.SetRange(CodigoLineas, rec.CodigoCabecera);
        if not rlLineasPlanVacunacion.IsEmpty then begin
            rlLineasPlanVacunacion.DeleteAll(true);
        end
    end;
}
